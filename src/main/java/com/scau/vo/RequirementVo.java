package com.scau.vo;

import java.sql.Timestamp;

public class RequirementVo {
	private Long id;// id
	private Long moduleId;// 所属模块id
	private int flag;//0表示正常，1表示已失效
	private Long testUserId;
	private String testUserName;//测试人员
	private Integer testFlag;//0表示未开始，1表示测试中，1表示已测试完成
	private String requirementName;// 需求名称

	private String moduleName;//模块名称
	private Long projectId;
	private Long userId;
	private String userName;//产品负责人
	private String projectName;//产品名称
	private Timestamp createTime;
	private Timestamp updateTime;




	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getModuleId() {
		return moduleId;
	}
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	public String getRequirementName() {
		return requirementName;
	}
	public void setRequirementName(String requirementName) {
		this.requirementName = requirementName;
	}
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getTestUserId() {
		return testUserId;
	}

	public void setTestUserId(Long testUserId) {
		this.testUserId = testUserId;
	}

	public Integer getTestFlag() {
		return testFlag;
	}

	public void setTestFlag(Integer testFlag) {
		this.testFlag = testFlag;
	}

	public String getTestUserName() {
		return testUserName;
	}

	public void setTestUserName(String testUserName) {
		this.testUserName = testUserName;
	}

	@Override
	public String toString() {
		return "RequirementVo{" +
				"id=" + id +
				", moduleId=" + moduleId +
				", moduleName='" + moduleName + '\'' +
				", projectId=" + projectId +
				", userId=" + userId +
				", userName='" + userName + '\'' +
				", projectName='" + projectName + '\'' +
				", requirementName='" + requirementName + '\'' +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				", testUserId=" + testUserId +
				", testUserName=" + testUserName +
				", testFlag=" + testFlag +
				'}';
	}
}
