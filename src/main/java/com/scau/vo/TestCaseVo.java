package com.scau.vo;

import java.sql.Timestamp;
import java.util.List;

public class TestCaseVo {
	private Long id;// id用例id
	private Long userId;
	private String user;//创建者姓名
	private Long userId2;
	private String user2;//执行者姓名
	private String title;// 用例标题
	private String prefix_condition;// 前置条件
	private String inputData;// 输入数据
	private String operation_steps;// 操作步骤
	private String expectation;// 预期结果
	private String remark;// 备注
	private Integer flag;//删除标记
	private Long num;//执行次数
	private Timestamp createTime;// 创建时间
	private Timestamp updateTime;// 最后编辑时间
	private Timestamp executeTime;// 最后执行时间

	private Long typeId;
	private String typeName;//用例类型
	private Long priorityId;
	private String priorityName;// 优先级
	private Long statusId;
	private String statusName;//用例状态
	private Long resultId;
	private String resultName;//执行结果
	private Long projectId;
	private String projectName;//产品
	private Long moduleId;
	private String moduleName;
	private Long requirementId;
	private String requirementName;
	private Long stageId;
	private String stageName;//适用阶段

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Long getUserId2() {
		return userId2;
	}

	public void setUserId2(Long userId2) {
		this.userId2 = userId2;
	}

	public String getUser2() {
		return user2;
	}

	public void setUser2(String user2) {
		this.user2 = user2;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPrefix_condition() {
		return prefix_condition;
	}

	public void setPrefix_condition(String prefix_condition) {
		this.prefix_condition = prefix_condition;
	}

	public String getInputData() {
		return inputData;
	}

	public void setInputData(String inputData) {
		this.inputData = inputData;
	}

	public String getOperation_steps() {
		return operation_steps;
	}

	public void setOperation_steps(String operation_steps) {
		this.operation_steps = operation_steps;
	}

	public String getExpectation() {
		return expectation;
	}

	public void setExpectation(String expectation) {
		this.expectation = expectation;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Long getNum() {
		return num;
	}

	public void setNum(Long num) {
		this.num = num;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Timestamp getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(Timestamp executeTime) {
		this.executeTime = executeTime;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Long getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}

	public String getPriorityName() {
		return priorityName;
	}

	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Long getResultId() {
		return resultId;
	}

	public void setResultId(Long resultId) {
		this.resultId = resultId;
	}

	public String getResultName() {
		return resultName;
	}

	public void setResultName(String resultName) {
		this.resultName = resultName;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Long getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(Long requirementId) {
		this.requirementId = requirementId;
	}

	public String getRequirementName() {
		return requirementName;
	}

	public void setRequirementName(String requirementName) {
		this.requirementName = requirementName;
	}

	public Long getStageId() {
		return stageId;
	}

	public void setStageId(Long stageId) {
		this.stageId = stageId;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	@Override
	public String toString() {
		return "TestCaseVo{" +
				"id=" + id +
				", userId=" + userId +
				", user='" + user + '\'' +
				", userId2=" + userId2 +
				", user2='" + user2 + '\'' +
				", title='" + title + '\'' +
				", prefix_condition='" + prefix_condition + '\'' +
				", inputData='" + inputData + '\'' +
				", operation_steps='" + operation_steps + '\'' +
				", expectation='" + expectation + '\'' +
				", remark='" + remark + '\'' +
				", flag=" + flag +
				", num=" + num +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				", executeTime=" + executeTime +
				", typeId=" + typeId +
				", typeName='" + typeName + '\'' +
				", priorityId=" + priorityId +
				", priorityName='" + priorityName + '\'' +
				", statusId=" + statusId +
				", statusName='" + statusName + '\'' +
				", resultId=" + resultId +
				", resultName='" + resultName + '\'' +
				", projectId=" + projectId +
				", projectName='" + projectName + '\'' +
				", moduleId=" + moduleId +
				", moduleName='" + moduleName + '\'' +
				", requirementId=" + requirementId +
				", requirementName='" + requirementName + '\'' +
				", stageId=" + stageId +
				", stageName='" + stageName + '\'' +
				'}';
	}
}
