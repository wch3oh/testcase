package com.scau.vo;

import java.sql.Timestamp;



public class RecordVo {
	private Long id;//id
	private String userName;//执行者姓名
	private String resultName;//执行结果
	private String truth;//实际情况
	private Timestamp updateTime;// 执行时间
	private String expectation;//预期




	@Override
	public String toString() {
		return "RecordVo [id=" + id + ", userName=" + userName + ", resultName=" + resultName + ", truth=" + truth
				+ ", updateTime=" + updateTime + "]";
	}

	public String getExpectation() {
		return expectation;
	}

	public void setExpectation(String expectation) {
		this.expectation = expectation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getResultName() {
		return resultName;
	}

	public void setResultName(String resultName) {
		this.resultName = resultName;
	}

	public String getTruth() {
		return truth;
	}

	public void setTruth(String truth) {
		this.truth = truth;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	
	
}
