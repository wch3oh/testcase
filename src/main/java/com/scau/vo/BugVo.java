package com.scau.vo;

import java.sql.Timestamp;

public class BugVo {
    private Long id;
    private Long requirementId;//所属需求
    private String bugType;//bug类型
    private Long soId;//bug的操作系统
    private Long browserId;//浏览器
    private String title;//标题
    private String operation_steps;//操作步骤
    private Long programmerId;//指派的开发人员id
    private Long userId;//创建bug人员id
    private Long priorityId;//优先级Id
    private Integer flag;//0=打开，1=已确认，2=已解决，3=已关闭
    private String solution;//解决方案
    private Long closeUserId;//关闭bug的人员
    private Timestamp closeTime;//bug关闭的时间
    private Long workUserId;//关闭bug的开发人员id

    private Long projectId;
    private String projectName;
    private Long moduleId;
    private String moduleName;
    private String requirementName;
    private String operatingSystem;
    private String browser;
    private String programmerName;
    private String userName;
    private String priorityName;
    private String closerUserName;
    private String workUserName;
    private Timestamp createTime;//bug创建时间
    private Timestamp updateTime;//更新时间

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }

    public String getBugType() {
        return bugType;
    }

    public void setBugType(String bugType) {
        this.bugType = bugType;
    }

    public Long getSoId() {
        return soId;
    }

    public void setSoId(Long soId) {
        this.soId = soId;
    }

    public Long getBrowserId() {
        return browserId;
    }

    public void setBrowserId(Long browserId) {
        this.browserId = browserId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOperation_steps() {
        return operation_steps;
    }

    public void setOperation_steps(String operation_steps) {
        this.operation_steps = operation_steps;
    }

    public Long getProgrammerId() {
        return programmerId;
    }

    public void setProgrammerId(Long programmerId) {
        this.programmerId = programmerId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Long priorityId) {
        this.priorityId = priorityId;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public Long getCloseUserId() {
        return closeUserId;
    }

    public void setCloseUserId(Long closeUserId) {
        this.closeUserId = closeUserId;
    }

    public Timestamp getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Timestamp closeTime) {
        this.closeTime = closeTime;
    }

    public Long getWorkUserId() {
        return workUserId;
    }

    public void setWorkUserId(Long workUserId) {
        this.workUserId = workUserId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getRequirementName() {
        return requirementName;
    }

    public void setRequirementName(String requirementName) {
        this.requirementName = requirementName;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getProgrammerName() {
        return programmerName;
    }

    public void setProgrammerName(String programmerName) {
        this.programmerName = programmerName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPriorityName() {
        return priorityName;
    }

    public void setPriorityName(String priorityName) {
        this.priorityName = priorityName;
    }

    public String getCloserUserName() {
        return closerUserName;
    }

    public void setCloserUserName(String closerUserName) {
        this.closerUserName = closerUserName;
    }

    public String getWorkUserName() {
        return workUserName;
    }

    public void setWorkUserName(String workUserName) {
        this.workUserName = workUserName;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "BugVo{" +
                "id=" + id +
                ", requirementId=" + requirementId +
                ", bugType='" + bugType + '\'' +
                ", soId=" + soId +
                ", browserId=" + browserId +
                ", title='" + title + '\'' +
                ", operation_steps='" + operation_steps + '\'' +
                ", programmerId=" + programmerId +
                ", userId=" + userId +
                ", priorityId=" + priorityId +
                ", flag=" + flag +
                ", solution='" + solution + '\'' +
                ", closeUserId=" + closeUserId +
                ", closeTime=" + closeTime +
                ", workUserId=" + workUserId +
                ", projectId=" + projectId +
                ", projectName='" + projectName + '\'' +
                ", moduleId=" + moduleId +
                ", moduleName='" + moduleName + '\'' +
                ", requirementName='" + requirementName + '\'' +
                ", operatingSystem='" + operatingSystem + '\'' +
                ", browser='" + browser + '\'' +
                ", programmerName='" + programmerName + '\'' +
                ", userName='" + userName + '\'' +
                ", priorityName='" + priorityName + '\'' +
                ", closerUserName='" + closerUserName + '\'' +
                ", workUserName='" + workUserName + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
