package com.scau.vo;

import java.sql.Timestamp;

public class ModuleVo {

	private Long id;// id
	private String projectName;//产品名称
	private String moduleName;// 模块名称
	private Timestamp createTime;//创建时间
	private Timestamp updateTime;//更新时间

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@Override
	public String toString() {
		return "ModuleVo{" +
				"id=" + id +
				", projectName='" + projectName + '\'' +
				", moduleName='" + moduleName + '\'' +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				'}';
	}
}
