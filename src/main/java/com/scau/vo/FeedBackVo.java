package com.scau.vo;

import java.sql.Timestamp;

public class FeedBackVo {
    private Long id;
    private Long userId;
    private String name;//用户姓名
    private String feedback; //反馈详情
    private String remark;//处理备注
    private Integer flag;//0表示未查看，1表示已查看，2表示已处理，3表示不予处理
    private Timestamp createTime;//创建时间
    private Timestamp updateTime;//更新时间

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "FeedBackVo{" +
                "id=" + id +
                ", userId=" + userId +
                ", name='" + name + '\'' +
                ", feedback='" + feedback + '\'' +
                ", remark='" + remark + '\'' +
                ", flag=" + flag +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
