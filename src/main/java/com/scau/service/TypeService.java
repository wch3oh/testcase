package com.scau.service;

import java.util.List;

import com.scau.model.Type;
import com.scau.vo.SelectVo;

public interface TypeService {

	/**
	 * 添加测试用例类型
	 * @param type
	 */
	long add(Type type);
	
	/**
	 * 删除类型
	 * @param type
	 */
	int delete(Type type);
	
	
	/**
	 * 修改类型
	 * @param type
	 */
	int update(Type type);

	/**
	 * 通过类型名查找（模糊搜索）
	 * @param typeName
	 */
	List<Type> search(String typeName);

	Type selectByTypeName(String typeName);

	/**
	 * 根据id获取类型信息
	 * @param typeId
	 * @return
	 */
	Type get(Long typeId);


	/**
	 * 获取所有商品类型
	 * @return
	 */
	List<Type> listAll();
	List<SelectVo> selectAll();
}
