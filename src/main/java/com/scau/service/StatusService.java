package com.scau.service;

import java.util.List;

import com.scau.model.Status;
import com.scau.vo.SelectVo;

public interface StatusService {

	/**
	 * 添加用例状态
	 * @param status
	 * @return
	 */
	long add(Status status);
	
	/**
	 * 根据id删除
	 * @param statusId
	 * @return
	 */
	int delete(Long statusId);
	
	/**
	 * 修改用例状态
	 * @param status
	 * @return
	 */
	int update(Status status);
	
	/**
	 * 通过状态名称查找（模糊搜索）
	 * @param statusName
	 * @return
	 */
	List<Status> search(String statusName);
	Status selectByName(String statusName);
	/**
	 * 根据id获取用例状态
	 * @param statusId
	 * @return
	 */
	Status get(Long statusId);
	
	/**
	 * 获取所有用例状态
	 * @return
	 */
	List<Status> listAll();
	List<SelectVo> selectAll();
}
