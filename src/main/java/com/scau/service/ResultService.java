package com.scau.service;

import java.util.List;

import com.scau.model.Result;
import com.scau.vo.SelectVo;

public interface ResultService {

	/**
	 * 添加执行结果
	 * @param result
	 * @return
	 */
	long add(Result result);
	/**
	 * 根据id删除
	 * @param resultId
	 * @return
	 */
	int delete(Long resultId);
	/**
	 * 修改执行结果信息
	 * @param result
	 * @return
	 */
	int update(Result result);
	/**
	 * 通过执行结果名查找（模糊搜索）
	 * @param resultName
	 * @return
	 */
	List<Result> search(String resultName);
	Result selectByName(String resultName);
	/**
	 * 根据id获取
	 * @param resultId
	 * @return
	 */
	Result get(Long resultId);
	/**
	 * 列出所有执行结果
	 * @return
	 */
	List<Result> listAll();
	List<SelectVo> selectAll();
}
