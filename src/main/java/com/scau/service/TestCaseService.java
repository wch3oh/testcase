package com.scau.service;

import java.util.List;


import com.scau.model.TestCase;
import com.scau.vo.TestCaseVo;

import javax.persistence.criteria.CriteriaBuilder;

public interface TestCaseService {
	
	/**
	 * 添加测试用例(flag默认为0）
	 * 
	 * @param testcase
	 */
	int add(TestCase testcase);

	/**
	 * 更新测试用例信息，flag=0
	 * 
	 * @param testcase
	 * @return
	 */
	int update(TestCase testcase);

	/**
	 * 更新flag
	 * 
	 * @param testcase
	 * @return
	 */
	int updateFlag(TestCase testcase);
	
	/**
	 * 删除测试用例（回收站中彻底删除）
	 * @param id
	 * @return
	 */
	int delete(Long id);


	/**
	 * 通过测试用例id查找测试用例
	 * 
	 * @param testcaseId
	 * @return
	 */
	TestCase selectByTestcaseId(Long testcaseId);
	TestCaseVo getByTestcaseId(Long testcaseId);

	/**
	 * 通过title查找
	 * @param title
	 * @param flag
	 * @return
	 */
	List<TestCase> selectByTitle(String title,Integer flag);
	List<TestCaseVo> listByTitle(String title,Integer flag);
	/**
	 * 根据用户id查找
	 * @return
	 */
	List<TestCaseVo> listByUserId(String title,Long userId,Integer flag);
	
	/**
	 * 根据类型id查找
	 * @return
	 */
	List<TestCaseVo> listByTypeId(String title,Long typeId,Integer flag);

	/**
	 * 根据优先级id查找
	 * @return
	 */
	List<TestCaseVo> listByPriorityId(String title,Long proprityId,Integer flag);

	/**
	 * 根据适用阶段id查找
	 * @param flag
	 * @param stageId
	 * @return
	 */
	List<TestCaseVo> listByStageId(String title,Long stageId,Integer flag);
	
	/**
	 * 根据测试用例状态查找
	 * @param flag
	 * @param statusId
	 * @return
	 */
	List<TestCaseVo> listByStatusId(String title,Long statusId,Integer flag);
	
	/**
	 * 根据执行结果查找
	 * @param resultId
	 * @param flag
	 * @return
	 */
	List<TestCaseVo> listByResultId(String title,Long resultId,Integer flag);
	
	/**
	 * 根据需求id查找
	 * @param requirementId
	 * @param flag
	 * @return
	 */
	List<TestCaseVo> listByRequirementId(String title,Long requirementId,Integer flag);

	
	/**
	 * 根据模块id查找
	 * @param moduleId
	 * @param flag
	 * @return
	 */
	List<TestCaseVo> listByModuleId(String title,Long moduleId,Integer flag);

	
	/**
	 * 根据产品id查找
	 * @param projectId
	 * @param flag
	 * @return
	 */
	List<TestCaseVo> listByProjectId(String title,Long projectId,Integer flag);

	
	/**
	 * 查找所有测试用例
	 * @param flag
	 * @return
	 */
	List<TestCaseVo> list(Integer flag);


	
}
