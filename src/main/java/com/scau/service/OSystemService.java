package com.scau.service;


import com.scau.model.OSystem;
import com.scau.vo.SelectVo;

import java.util.List;

public interface OSystemService {
    /**
     * 添加浏览器
     * @param osystem
     */
    long add(OSystem osystem);

    /**
     * 删除浏览器
     * @param osystem
     */
    int delete(OSystem osystem);


    /**
     * 修改浏览器
     * @param osystem
     */
    int update(OSystem osystem);

    /**
     * 通过名称查找（模糊搜索）
     * @param operatingSystem
     */
    List<OSystem> search(String operatingSystem);

    OSystem selectByOSystemName(String operatingSystem);

    /**
     * 根据id获取浏览器
     * @param osystemId
     * @return
     */
    OSystem get(Long osystemId);


    /**
     * 获取所有浏览器
     * @return
     */
    List<OSystem> listAll();
    List<SelectVo> selectAll();
}
