package com.scau.service;

import java.util.List;
import com.scau.model.Stage;
import com.scau.vo.SelectVo;

public interface StageService {
	/**
	 * 添加测试用例适用阶段
	 * @param stage
	 */
	long add(Stage stage);
	
	/**
	 * 删除
	 * @param stageId
	 */
	int delete(Long stageId);
	
	
	/**
	 * 修改
	 * @param stage
	 */
	int update(Stage stage);

	/**
	 * 通过适用阶段名称查找（模糊搜索）
	 * @param stageName
	 */
	List<Stage> search(String stageName);
	Stage selectByName(String stageName);
	/**
	 * 根据id获取
	 * @param stageId
	 * @return
	 */
	Stage get(Long stageId);


	/**
	 * 获取所有适用阶段
	 * @return
	 */
	List<Stage> listAll();
	List<SelectVo> selectAll();
}
