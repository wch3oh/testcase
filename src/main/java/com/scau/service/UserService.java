package com.scau.service;
import java.util.List;
import com.scau.model.User;
import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Select;

public interface UserService {

	
	/**
	 * 添加用户
	 * @param user
	 */
	Long add(User user);

	/**
	 * 删除用户
	 * @param userId
	 */
	int delete(Long userId);

	/**
	 * 更新用户信息
	 * @param user
	 * @return
	 */
    int update(User user);
	/**
	 * 更改密码
	 * @param 
	 */
	int updatePassword(User user);
	
	/**
	 * 根据id获取用户信息
	 * @param id
	 * @return
	 */
	User get(Long id);
	
	/**
	 * 根据账号和密码 查询用户
	 * @param account
	 * @param password
	 * @return
	 */
	User search(String account,String password);
	
	/**
	 * 根据账号查询用户
	 * @param account
	 * @return
	 */
	User search(String account);
	
	/**
	 * 获取全部用户信息
	 * @return List<User>
	 */
	List<User> list(String name,String sex,Integer flag);
	List<SelectVo> selectAll();
}
