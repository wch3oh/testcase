package com.scau.service;

import com.scau.model.Browser;
import com.scau.vo.SelectVo;

import java.util.List;

public interface BrowserService {
    /**
     * 添加浏览器
     * @param browser
     */
    long add(Browser browser);

    /**
     * 删除浏览器
     * @param browser
     */
    int delete(Browser browser);


    /**
     * 修改浏览器
     * @param browser
     */
    int update(Browser browser);

    /**
     * 通过名称查找（模糊搜索）
     * @param browserName
     */
    List<Browser> search(String browserName);

    Browser selectByBrowserName(String browserName);

    /**
     * 根据id获取浏览器
     * @param browserId
     * @return
     */
    Browser get(Long browserId);


    /**
     * 获取所有浏览器
     * @return
     */
    List<Browser> listAll();
    List<SelectVo> selectAll();
}
