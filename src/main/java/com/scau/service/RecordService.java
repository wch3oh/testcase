package com.scau.service;

import java.util.List;

import com.scau.model.Record;
import com.scau.vo.RecordVo;
import com.scau.vo.SelectVo;

public interface RecordService {

	/**
	 * 添加执行记录
	 * @param record
	 * @return
	 */
	Long add(Record record);
	
	Record get(Long recordId);
	
	/**
	 * 根据测试用例id获取执行记录
	 * @param testcaseId
	 * @return
	 */
	//List<Record> list(Long testcaseId);
	List<RecordVo> list(Long testcaseId);
	List<SelectVo> selectAll();
}
