package com.scau.service.impl;

import java.util.List;

import com.scau.vo.RecordVo;
import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scau.mapper.RecordMapper;
import com.scau.model.Record;
import com.scau.service.RecordService;

@Service("recordService")
public class RecordServiceImpl implements RecordService {

	@Autowired 
	private RecordMapper recordMapper;
	
	/**
	 * 添加执行记录
	 */
	@Override
	public Long add(Record record) {
		recordMapper.insertSelective(record);
		return record.getId();
	}

	/**
	 * 根据测试用例id获取执行记录
	 */
	@Override
	public List<RecordVo> list(Long testcaseId) {
		return recordMapper.selectByTestCaseId(testcaseId);
	}

	@Override
	public Record get(Long recordId) {
		return recordMapper.selectByRecordId(recordId);
	}

	@Override
	public List<SelectVo> selectAll() {
		return recordMapper.selectAll();
	}


}
