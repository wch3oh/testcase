package com.scau.service.impl;

import java.util.List;

import com.scau.vo.TestCaseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scau.mapper.TestCaseMapper;
import com.scau.model.TestCase;
import com.scau.service.TestCaseService;

@Service("testcaseService")
public class TestCaseServiceImpl implements TestCaseService{

	@Autowired 
	private TestCaseMapper testcaseMapper;
	
	/**
	 * 添加测试用例
	 */
	@Override
	public int add(TestCase testcase) {
		return testcaseMapper.insert(testcase);
	}

	/**
	 * 更新测试用例信息
	 */
	@Override
	public int update(TestCase testcase) {
		return testcaseMapper.update(testcase);
	}

	/**
	 * 修改flag
	 */
	@Override
	public int updateFlag(TestCase testcase) {
		return testcaseMapper.updateFlag(testcase);
	}

	/**
	 * 通过id删除
	 */
	@Override
	public int delete(Long id) {
		return testcaseMapper.delete(id);
	}

	/**
	 * 根据id获取
	 */
	@Override
	public TestCase selectByTestcaseId(Long testcaseId) {
		return testcaseMapper.selectByTestcaseId(testcaseId);
	}

	@Override
	public TestCaseVo getByTestcaseId(Long testcaseId) {
		return testcaseMapper.getByTestcaseId(testcaseId);
	}

	/**
	 * 通过title查询
	 */
	@Override
    public List<TestCase> selectByTitle(String title,Integer flag){
		return testcaseMapper.selectByTitle(title,flag);
	}

	@Override
	public List<TestCaseVo> listByTitle(String title, Integer flag) {
		return testcaseMapper.listByTitle(title,flag);
	}

	/**
	 * 根据创建者id获取
	 */
	@Override
	public List<TestCaseVo> listByUserId(String title,Long userId,Integer flag) {
		return testcaseMapper.listByUserId(title,userId,flag);
	}

	/**
	 * 根据用例类型获取
	 */
	@Override
	public List<TestCaseVo> listByTypeId(String title,Long typeId,Integer flag) {
		return testcaseMapper.listByTypeId(title,typeId,flag);
	}

	/**
	 * 根据优先级id获取
	 */
	@Override
	public List<TestCaseVo> listByPriorityId(String title,Long proprityId,Integer flag) {
		return testcaseMapper.listByPriorityId(title,proprityId,flag);
	}


	/**
	 * 根据用例适用阶段id获取
	 */
	@Override
	public List<TestCaseVo> listByStageId(String title,Long stageId,Integer flag) {
		return testcaseMapper.listByStageId(title,stageId,flag);
	}


	/**
	 * 根据用例状态id获取
	 */
	@Override
	public List<TestCaseVo> listByStatusId(String title,Long statusId,Integer flag) {
		return testcaseMapper.listByStatusId(title,statusId,flag);
	}

	/**
	 * 根据执行结果id获取
	 */
	@Override
	public List<TestCaseVo> listByResultId(String title,Long resultId,Integer flag) {
		return testcaseMapper.listByResultId(title,resultId,flag);
	}


	/**
	 * 根据需求id获取
	 */
	@Override
	public List<TestCaseVo> listByRequirementId(String title,Long requirementId,Integer flag) {
		return testcaseMapper.listByRequirementId(title,requirementId,flag);
	}


	/**
	 * 根据模块id获取
	 */
	@Override
	public List<TestCaseVo> listByModuleId(String title,Long moduleId,Integer flag) {
		return testcaseMapper.listByModuleId(title,moduleId,flag);
	}

	/**
	 * 根据产品id获取
	 * @param title
	 * @param projectId
	 * @param flag
	 * @return
	 */
	@Override
	public List<TestCaseVo> listByProjectId(String title,Long projectId, Integer flag) {
		return testcaseMapper.listByProjectId(title,projectId,flag);
	}


	/**
	 * 获取所有测试用例
	 */
	@Override
	public List<TestCaseVo> list(Integer flag) {
		return testcaseMapper.list(flag);
	}

	

}
