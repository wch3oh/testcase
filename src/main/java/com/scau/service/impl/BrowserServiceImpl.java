package com.scau.service.impl;

import com.scau.mapper.BrowserMapper;
import com.scau.model.Browser;
import com.scau.service.BrowserService;
import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("BrowserService")
public class BrowserServiceImpl implements BrowserService {
    @Autowired
    private BrowserMapper browserMapper;

    @Override
    public long add(Browser browser) {
         browserMapper.insertSelective(browser);
         return browser.getId();
    }

    @Override
    public int delete(Browser browser) {
        return browserMapper.delete(browser);
    }

    @Override
    public int update(Browser browser) {
        return browserMapper.update(browser);
    }

    @Override
    public List<Browser> search(String browserName) {
        return browserMapper.listByName(browserName);
    }

    @Override
    public Browser selectByBrowserName(String browserName) {
        return browserMapper.selectByName(browserName);
    }

    @Override
    public Browser get(Long browserId) {
        return browserMapper.selectByBrowserId(browserId);
    }

    @Override
    public List<Browser> listAll() {
        return browserMapper.list();
    }

    @Override
    public List<SelectVo> selectAll() {
        return browserMapper.selectAll();
    }

}
