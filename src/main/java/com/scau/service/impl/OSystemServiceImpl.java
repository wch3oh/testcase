package com.scau.service.impl;

import com.scau.mapper.OSystemMapper;
import com.scau.model.OSystem;
import com.scau.service.OSystemService;
import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("OSystemService")
public class OSystemServiceImpl implements OSystemService {
    @Autowired
    private OSystemMapper oSystemMapper;
    @Override
    public long add(OSystem osystem) {
        return oSystemMapper.insertSelective(osystem);
    }

    @Override
    public int delete(OSystem osystem) {
        return oSystemMapper.delete(osystem);
    }

    @Override
    public int update(OSystem osystem) {
        return oSystemMapper.update(osystem);
    }

    @Override
    public List<OSystem> search(String operatingSystem) {
        return oSystemMapper.listByName(operatingSystem);
    }

    @Override
    public OSystem selectByOSystemName(String operatingSystem) {
        return oSystemMapper.selectByName(operatingSystem);
    }

    @Override
    public OSystem get(Long osystemId) {
        return oSystemMapper.selectByOSystemId(osystemId);
    }

    @Override
    public List<OSystem> listAll() {
        return oSystemMapper.list();
    }

    @Override
    public List<SelectVo> selectAll() {
        return oSystemMapper.selectAll();
    }
}
