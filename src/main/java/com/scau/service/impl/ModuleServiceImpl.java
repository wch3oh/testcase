package com.scau.service.impl;

import java.util.List;

import com.scau.vo.ModuleVo;
import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scau.mapper.ModuleMapper;
import com.scau.mapper.RequirementMapper;
import com.scau.model.Module;
import com.scau.service.ModuleService;

@Service("moduleService")
public class ModuleServiceImpl implements ModuleService {

	@Autowired
	private ModuleMapper moduleMapper;
	
	/**
	 * 添加模块
	 */
	@Override
	public int add(Module module) {
		return moduleMapper.insert(module);
	}
    
	/**
	 * 更新模块信息
	 */
	@Override
	public int update(Module module) {
		return moduleMapper.update(module);
	}

	/**
	 * 根据id删除
	 */
	@Override
	public int delete(Long moduleId) {
		return moduleMapper.delete(moduleId);
	}

	/**
	 * 通过模块名称查找
	 */
	@Override
	public Module selectByName(String moduleName,Long projectId) {
		return moduleMapper.selectByModuleName(moduleName,projectId);
	}

	/**
	 * 根据产品id获取模块列表
	 * @param projectId
	 * @return
	 */
	@Override
	public List<Module> listByProjectId(Long projectId) {
		return moduleMapper.listByProjectId(projectId);
	}

	/**
	 * 获取模块列表
	 */
	public List<ModuleVo>list(Long projectId,String moduleName,Long userId){
		return moduleMapper.list(projectId,moduleName,userId);
	}

	/**
	 * 根据id获取
	 * @param moduleId
	 * @return
	 */
	@Override
	public Module get(Long moduleId) {
		return moduleMapper.selectByModuleId(moduleId);
	}

	@Override
	public List<SelectVo> selectAll() {
		return moduleMapper.selectAll();
	}

}
