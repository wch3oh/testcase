package com.scau.service.impl;

import java.util.List;

import com.scau.vo.ProjectVo;
import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scau.mapper.ModuleMapper;
import com.scau.mapper.ProjectMapper;
import com.scau.model.Project;
import com.scau.service.ProjectService;

@Service("projectService")
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectMapper projectMapper;
	
	/**
	 * 添加产品
	 */
	@Override
	public long add(Project project) {
		projectMapper.insertSelective(project);
		return project.getId();
	}

	/**
	 * 根据id删除
	 */
	@Override
	public int delete(Long projectId) {
		return projectMapper.delete(projectId);
	}

	/**
	 * 修改产品信息
	 */
	@Override
	public int update(Project project) {		
		return projectMapper.update(project);
	}

	/**
	 * 通过产品名称查找
	 */
	@Override
	public List<ProjectVo> list(String projectName,Long userId) {
		return projectMapper.list(projectName,userId);
	}
	@Override
	public Project selectByName(String projectName) {
		return projectMapper.selectByName(projectName);
	}

	/**
	 * 通过id获取
	 */
	@Override
	public Project get(Long projectId) {
		return projectMapper.selectByProjectId(projectId);
	}


	@Override
	public List<SelectVo> selectAll() {
		return projectMapper.selectAll();
	}

}
