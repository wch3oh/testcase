package com.scau.service.impl;

import com.scau.mapper.FeedBackMapper;
import com.scau.model.FeedBack;
import com.scau.service.FeedBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("feedbackService")
public class FeedBackServiceImpl implements FeedBackService {

    @Autowired
    private FeedBackMapper feedbackMapper;

    @Override
    public long add(FeedBack feedback) {
        return feedbackMapper.insertSelective(feedback);
    }

    @Override
    public int delete(Long id) {
        return feedbackMapper.delete(id);
    }

    @Override
    public int update(FeedBack feedback) {
        return feedbackMapper.update(feedback);
    }

    @Override
    public List<FeedBack> list(Long userId,Integer flag) {
        return feedbackMapper.list(userId,flag);
    }

    @Override
    public FeedBack get(Long id) {
        return feedbackMapper.selectById(id);
    }
}
