package com.scau.service.impl;

import java.util.List;

import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.scau.mapper.TestCaseMapper;
import com.scau.mapper.TypeMapper;
import com.scau.model.Type;
import com.scau.service.TypeService;

@Service("typeService")
public class TypeServiceImpl implements TypeService {

	@Autowired
	private TypeMapper typeMapper;
	
	/**
	 * 添加类型
	 */
	@Override
	public long add(Type type) {
		typeMapper.insertSelective(type);
		return type.getId();
	}

	/**
	 * 根据id删除类型
	 */
	@Override
	public int delete(Type type) {
		return typeMapper.delete(type);
	}
	
	/**
	 * 修改类型信息
	 */
	@Override
	public int update(Type type) {
		return typeMapper.update(type);
	}

	/**
	 * 根据id查找类型信息
	 */
	@Override
	public Type get(Long typeId) {
		return typeMapper.selectByTypeId(typeId);
	}


	/**
	 * 通过类型名查找（模糊搜索）
	 */
	@Override
	public List<Type> search(String typeName) {
		return typeMapper.listByTypeName(typeName);
	}

	@Override
	public Type selectByTypeName(String typeName) {
		return typeMapper.selectByTypeName(typeName);
	}

	/**
	 * 获取所有类型
	 */
	@Override
	public List<Type> listAll() {
		return typeMapper.list();
	}

	@Override
	public List<SelectVo> selectAll() {
		return typeMapper.selectAll();
	}


}