package com.scau.service.impl;

import java.util.List;

import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.scau.mapper.StageMapper;
import com.scau.mapper.TestCaseMapper;
import com.scau.model.Stage;
import com.scau.service.StageService;

@Service("stageService")
public class StageServiceImpl implements StageService{

	@Autowired
	private StageMapper stageMapper;
	
	@Override
	public long add(Stage stage) {
		stageMapper.insertSelective(stage);
		return stage.getId();
	}

	@Override
	public int delete(Long stageId) {
		return stageMapper.delete(stageId);
	}

	@Override
	public int update(Stage stage) {
		return stageMapper.update(stage);
	}

	/**
	 * 通过适用阶段名称查找
	 * @param stageName
	 * @return
	 */
	@Override
	public List<Stage> search(String stageName) {
		return stageMapper.listByName(stageName);
	}
	@Override
	public Stage selectByName(String stageName) {
		return stageMapper.selectByName(stageName);
	}

	/**
	 * 通过id获取
	 * @param stageId
	 * @return
	 */
	@Override
	public Stage get(Long stageId) {
		return stageMapper.selectByStageId(stageId);
	}

	/**
	 * 获取适用阶段列表
	 * @return
	 */
	@Override
	public List<Stage> listAll() {
		return stageMapper.list();
	}

	@Override
	public List<SelectVo> selectAll() {
		return stageMapper.selectAll();
	}

}
