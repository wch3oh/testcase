package com.scau.service.impl;

import java.util.List;

import com.scau.vo.RequirementVo;
import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scau.mapper.RequirementMapper;
import com.scau.mapper.TestCaseMapper;
import com.scau.model.Requirement;
import com.scau.service.RequirementService;

@Service("requirementService")
public class RequirementServiceImpl implements RequirementService {

	@Autowired
	private RequirementMapper requirementMapper;

	/**
	 * 添加需求
	 */
	@Override
	public long add(Requirement requirement) {
		return requirementMapper.insert(requirement);
	}

	/**
	 * 删除需求
	 */
	@Override
	public int delete(Long requirementId) {
		return requirementMapper.delete(requirementId);
	}

	/**
	 * 修改需求信息
	 */
	@Override
	public int update(Requirement requirement) {
		return requirementMapper.update(requirement);
	}

	/**
	 * 通过需求名查找（模糊搜索）
	 */
	@Override
	public List<RequirementVo> list(Integer testFlag,Long projectId,Long moduleId,String requirementName,Long testUserId,Long userId) {
		return requirementMapper.list(testFlag,projectId,moduleId,requirementName,testUserId,userId);
	}

	@Override
	public Requirement selectByName(Long moduleId,String requirementName) {
		return requirementMapper.selectByName(moduleId,requirementName);
	}


	;
	/**
	 * 通过id查找
	 */
	@Override
	public Requirement get(Long requirementId) {
		return requirementMapper.selectByRequirementId(requirementId);
	}
	@Override
	public RequirementVo selectById(Long requirementId) {
		return requirementMapper.selectById(requirementId);
	}


	@Override
	public List<RequirementVo> selectByModuleId(Long moduleId) {
		return requirementMapper.selectByModuleId(moduleId);
	}

	@Override
	public List<RequirementVo> selectByProductId(Long productId) {
		return null;
	}

	@Override
	public List<SelectVo> selectAll() {
		return requirementMapper.selectAll();
	}


}
