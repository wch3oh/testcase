package com.scau.service.impl;

import java.util.List;

import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scau.mapper.ResultMapper;
import com.scau.mapper.TestCaseMapper;
import com.scau.model.Result;
import com.scau.service.ResultService;

@Service("resultService")
public class ResultServiceImpl implements ResultService{

	@Autowired
	private ResultMapper resultMapper;
	/**
	 * 添加执行结果
	 */
	@Override
	public long add(Result result) {
		resultMapper.insertSelective(result);
		return result.getId();
	}

	/**
	 * 根据id删除
	 */
	@Override
	public int delete(Long resultId) {
		return resultMapper.delete(resultId);
	}

	/**
	 * 修改执行结果信息
	 */
	@Override
	public int update(Result result) {
		return resultMapper.update(result);
	}

	/**
	 * 根据名称查找（模糊搜索）
	 */
	@Override
	public List<Result> search(String resultName) {
		return resultMapper.listByName(resultName);
	}

	@Override
	public Result selectByName(String resultName) {
		return resultMapper.selectByName(resultName);
	}

	/**
	 * 根据id获取
	 */
	@Override
	public Result get(Long resultId) {
		return resultMapper.selectByResultId(resultId);
	}

	/**
	 * 获取执行结果列表
	 */
	@Override
	public List<Result> listAll() {
		return resultMapper.list();
	}


	@Override
	public List<SelectVo> selectAll() {
		return resultMapper.selectAll();
	}

}
