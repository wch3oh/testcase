package com.scau.service.impl;
import java.util.List;

import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.scau.mapper.UserMapper;
import com.scau.model.User;
import com.scau.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;

	/**
	 * 添加用户
	 */
	@Override
	public Long add(User user) {
		return userMapper.insert(user);
	}

	/**
	 * 删除用户
	 */
	@Override
	public int delete(Long userId) {
		return userMapper.delete(userId);
	}

	@Override
	public int update(User user) {
		return userMapper.update(user);
	}

	/**
	 * 根据用户id获取用户信息
	 */
	@Override
	public User get(Long id) {
		return userMapper.selectByUserId(id);
	}

	
	/**
	 * 根据账号和密码查询用户
	 */
	@Override
	public User search(String account, String password) {
		return userMapper.selectByAccountAndPassword(account, password);
	}

	/**
	 * 根据账号查询用户
	 */
	@Override
	public User search(String account) {
		return userMapper.selectByAccount(account);
	}

	/**
	 * 获取所有用户信息
	 */

	@Override
	public List<User> list(String name, String sex, Integer flag) {
		return userMapper.list(name,sex,flag);
	}

	@Override
	public List<SelectVo> selectAll() {
		return userMapper.selectAll();
	}

	/**
	 * 更改密码
	 */
	@Override
	public int updatePassword(User user) {
		return userMapper.updatePassword(user);
	}



}
