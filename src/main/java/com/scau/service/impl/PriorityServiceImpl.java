package com.scau.service.impl;

import java.util.List;

import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scau.mapper.PriorityMapper;
import com.scau.mapper.TestCaseMapper;
import com.scau.model.Priority;
import com.scau.service.PriorityService;

@Service("priorityService")
public class PriorityServiceImpl implements PriorityService{

	@Autowired
	private PriorityMapper priorityMapper;
	
	/**
	 * 添加优先级
	 */
	@Override
	public long add(Priority priority) {
		priorityMapper.insertSelective(priority);
		return priority.getId();
	}

	/**
	 * 根据id删除
	 */
	@Override
	public int delete(Long priorityId) {
		return priorityMapper.delete(priorityId);
	}

	/**
	 * 修改优先级信息
	 */
	@Override
	public int update(Priority priority) {		
		return priorityMapper.update(priority);
	}

	/**
	 * 通过优先级名查找
	 */
	@Override
	public List<Priority> search(String priorityName) {
		return priorityMapper.listByName(priorityName);
	}
	@Override
	public Priority selectByName(String priorityName) {
		return priorityMapper.selectByName(priorityName);
	}

	/**
	 * 通过id获取
	 */
	@Override
	public Priority get(Long priorityId) {
		return priorityMapper.selectByPriorityId(priorityId);
	}

	/**
	 * 获取优先级列表
	 * @return
	 */
	@Override
	public List<Priority> listAll() {
		return priorityMapper.list();
	}

	@Override
	public List<SelectVo> selectAll() {
		return priorityMapper.selectAll();
	}

}
