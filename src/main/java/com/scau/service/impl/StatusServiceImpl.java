package com.scau.service.impl;

import java.util.List;

import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scau.mapper.StatusMapper;
import com.scau.mapper.TestCaseMapper;
import com.scau.model.Status;
import com.scau.service.StatusService;

@Service("statusService")
public class StatusServiceImpl implements StatusService {

	@Autowired
	private StatusMapper statusMapper;
	
	/**
	 * 添加状态
	 */
	@Override
	public long add(Status status) {
		statusMapper.insertSelective(status);
		return status.getId();
	}

	/**
	 * 根据id删除状态
	 */
	@Override
	public int delete(Long statusId) {
		return statusMapper.delete(statusId);
	}

	/**
	 * 修改状态信息
	 */
	@Override
	public int update(Status status) {
		return statusMapper.update(status);
	}

	/**
	 * 通过状态名查找（模糊搜索）
	 */
	@Override
	public List<Status> search(String statusName) {
		return statusMapper.listByName(statusName);
	}
	@Override
	public Status selectByName(String statusName) {
		return statusMapper.selectByName(statusName);
	}

	/**
	 * 根据id获取
	 */
	@Override
	public Status get(Long statusId) {		
		return statusMapper.selectByStatusId(statusId);
	}

	/**
	 * 获取所有
	 */
	@Override
	public List<Status> listAll() {
		return statusMapper.list();
	}


	@Override
	public List<SelectVo> selectAll() {
		return statusMapper.selectAll();
	}

}
