package com.scau.service;

import java.util.List;

import com.scau.model.Project;
import com.scau.vo.ProjectVo;
import com.scau.vo.SelectVo;


public interface ProjectService {

	/**
	 * 添加产品（项目）
	 * @param project
	 * @return
	 */
	long add(Project project);
	/**
	 * 通过id删除
	 * @param projectId
	 * @return
	 */
	int delete(Long projectId);
	/**
	 * 修改产品名称
	 * @param project
	 * @return
	 */
	int update(Project project);

	List<ProjectVo> list(String projectName, Long userId);
	Project selectByName(String projectName);
	/**
	 * 通过id获取
	 * @param projectId
	 * @return
	 */
	Project get(Long projectId);

	List<SelectVo> selectAll();
}
