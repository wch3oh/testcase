package com.scau.service;

import com.scau.model.FeedBack;

import java.util.List;

public interface FeedBackService {
    /**
     * 添加反馈
     * @param feedback
     * @return
     */
    long add(FeedBack feedback);

    /**
     * 根据id删除
     * @param id
     */
    int delete(Long id);

    /**
     * 修改
     * @param feedback
     * @return
     */
    int update(FeedBack feedback);

    /**
     * 获取所有反馈
     * @return
     */
    List<FeedBack> list(Long userId,Integer flag);
    /**
     * 通过id获取
     * @param id
     * @return
     */
    FeedBack get(Long id);
}
