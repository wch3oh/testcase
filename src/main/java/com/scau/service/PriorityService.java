package com.scau.service;

import java.util.List;

import com.scau.model.Priority;
import com.scau.vo.SelectVo;

public interface PriorityService {

	/**
	 * 添加优先级
	 * @param priority
	 * @return
	 */
	long add(Priority priority);
	/**
	 * 根据id删除
	 * @param priorityId
	 * @return
	 */
	int delete(Long priorityId);
	/**
	 * 修改优先级信息
	 * @param priority
	 * @return
	 */
	int update(Priority priority);
	/**
	 * 通过优先级名称查找（模糊搜索）
	 * @param priorityName
	 * @return
	 */
	List<Priority> search(String priorityName);
	Priority selectByName(String priorityName);
	/**
	 * 通过id获取
	 * @param priorityId
	 * @return
	 */
	Priority get(Long priorityId);
	/**
	 * 获取优先级列表
	 * @return
	 */
	List<Priority> listAll();
	List<SelectVo> selectAll();
}
