package com.scau.service;

import java.util.List;

import com.scau.model.Module;
import com.scau.vo.ModuleVo;
import com.scau.vo.SelectVo;

public interface ModuleService {
	/**
	 * 添加模块
	 * 
	 * @param module
	 * @return
	 */
	int add(Module module);

	/**
	 * 更新模块信息
	 * 
	 * @param module
	 * @return
	 */
	int update(Module module);

	/**
	 * 通过id删除
	 * 
	 * @param moduleId
	 * @return
	 */
	int delete(Long moduleId);

	/**
	 * 通过名称查找
	 * 
	 * @param moduleName
	 * @return
	 */
	Module selectByName(String moduleName,Long projectId);

	/**
	 * 获取模块列表
	 * 
	 * @return
	 */

	List<Module> listByProjectId(Long projectId);
	List<ModuleVo>list(Long projectId,String moduleName,Long userId);
	/**
	 * 根据id获取
	 * @param moduleId
	 * @return
	 */
	Module get(Long moduleId);
    List<SelectVo> selectAll();
}
