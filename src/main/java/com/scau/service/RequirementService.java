package com.scau.service;

import java.util.List;

import com.scau.model.Requirement;
import com.scau.vo.RequirementVo;
import com.scau.vo.SelectVo;

public interface RequirementService {

	/**
	 * 添加需求
	 * 
	 * @param requirement
	 * @return
	 */
	long add(Requirement requirement);

	/**
	 * 删除需求
	 * 
	 * @param requirementId
	 * @return
	 */
	int delete(Long requirementId);

	/**
	 * 修改需求信息
	 * 
	 * @param requirement
	 * @return
	 */
	int update(Requirement requirement);

	/**
	 * 通过id获取
	 * 
	 * @param requirementId
	 * @return
	 */
	Requirement get(Long requirementId);
	RequirementVo selectById(Long requirementId);

	/**
	 * 通过需求名称查找（模糊搜索）
	 * 
	 * @param requirementName
	 * @return
	 */
	List<RequirementVo> list(Integer Flag,Long projectId,Long moduleId,String requirementName,Long testUserId,Long userId);
	Requirement selectByName(Long moduleId,String requirementName);

	List<RequirementVo> selectByModuleId(Long moduleId);

	List<RequirementVo> selectByProductId(Long productId);
	List<SelectVo> selectAll();
}
