package com.scau.model;

import org.apache.ibatis.type.Alias;

@Alias("User")
public class User extends BaseModel {


	private String account;//账号
	private String nickname;//昵称
	private String password;//密码
	private Integer positionFlag;//0表示用户，1表示管理员
	private Integer deleteFlag;//是否删除
    private Integer authorityFlag;//权限
	private String name;//姓名
	private String phone;//联系方式
	private String sex;//性别
	private String email;//邮箱
	private String image;//头像

	public Integer getPositionFlag() {
		return positionFlag;
	}

	public void setPositionFlag(Integer positionFlag) {
		this.positionFlag = positionFlag;
	}

	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public Integer getAuthorityFlag() {
		return authorityFlag;
	}

	public void setAuthorityFlag(Integer authorityFlag) {
		this.authorityFlag = authorityFlag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "User{" +
				"account='" + account + '\'' +
				", nickname='" + nickname + '\'' +
				", password='" + password + '\'' +
				", positionFlag=" + positionFlag +
				", deleteFlag=" + deleteFlag +
				", authorityFlag=" + authorityFlag +
				", name='" + name + '\'' +
				", phone='" + phone + '\'' +
				", sex='" + sex + '\'' +
				", email='" + email + '\'' +
				", image='" + image + '\'' +
				", id=" + id +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				'}';
	}
}
