package com.scau.model;

import org.apache.ibatis.type.Alias;
//需求
@Alias("Requirement")
public class Requirement extends BaseModel {
	private Long moduleId;
	private String requirementName;
	private Integer flag;//0表示正常，1表示失效-逻辑删除；
    private Integer testFlag;//0表示未开始，1表示测试中，2表示已测试完成
	private Long testUserId;//指派的测试人员id

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public String getRequirementName() {
		return requirementName;
	}

	public void setRequirementName(String requirementName) {
		this.requirementName = requirementName;
	}

	public Integer getTestFlag() {
		return testFlag;
	}

	public void setTestFlag(Integer testFlag) {
		this.testFlag = testFlag;
	}

	public Long getTestUserId() {
		return testUserId;
	}

	public void setTestUserId(Long testUserId) {
		this.testUserId = testUserId;
	}

	@Override
	public String toString() {
		return "Requirement{" +
				"moduleId=" + moduleId +
				", requirementName='" + requirementName + '\'' +
				", flag=" + flag +
				", testFlag=" + testFlag +
				", testUserId=" + testUserId +
				", id=" + id +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				'}';
	}
}
