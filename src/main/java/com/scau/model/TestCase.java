package com.scau.model;

import java.sql.Timestamp;

import org.apache.ibatis.type.Alias;

//测试用例
@Alias("TestCase")
public class TestCase extends BaseModel{
	private Long userId;//创建者id
	private Long userId2;//最后执行者id
	private Long requirementId;// 所属需求
	private Long typeId;// 用例类型
	private Long stageId;// 适用阶段
	private Long statusId;// 用例状态,默认正常
	private Long resultId;//最后一次执行结果，默认未执行
	private String title;// 用例标题
	private String prefix_condition;// 前置条件
	private String inputData;// 输入数据
	private String operation_steps;// 操作步骤
	private String expectation;// 预期结果
	private String remark;// 备注
	private Long priorityId;// 优先级
	private Timestamp updateTime;// 最后编辑时间
	private Timestamp executeTime;// 最后执行时间
	private Integer flag;// 0表示正常，1表示处于回收站
    private Long num;//执行次数

	public Long getNum() {
		return num;
	}

	public void setNum(Long num) {
		this.num = num;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserId2() {
		return userId2;
	}

	public void setUserId2(Long userId2) {
		this.userId2 = userId2;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPrefix_condition() {
		return prefix_condition;
	}

	public void setPrefix_condition(String prefix_condition) {
		this.prefix_condition = prefix_condition;
	}

	public String getInputData() {
		return inputData;
	}

	public void setInputData(String inputData) {
		this.inputData = inputData;
	}

	public String getOperation_steps() {
		return operation_steps;
	}

	public void setOperation_steps(String operation_steps) {
		this.operation_steps = operation_steps;
	}

	public String getExpectation() {
		return expectation;
	}

	public void setExpectation(String expectation) {
		this.expectation = expectation;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}	
	
	public Long getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(Long requirementId) {
		this.requirementId = requirementId;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public Long getStageId() {
		return stageId;
	}

	public void setStageId(Long stageId) {
		this.stageId = stageId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getResultId() {
		return resultId;
	}

	public void setResultId(Long resultId) {
		this.resultId = resultId;
	}

	public Long getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Override
	public Timestamp getUpdateTime() {
		return updateTime;
	}

	@Override
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Timestamp getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(Timestamp executeTime) {
		this.executeTime = executeTime;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "TestCase{" +
				"userId=" + userId +
				", userId2=" + userId2 +
				", requirementId=" + requirementId +
				", typeId=" + typeId +
				", stageId=" + stageId +
				", statusId=" + statusId +
				", resultId=" + resultId +
				", title='" + title + '\'' +
				", prefix_condition='" + prefix_condition + '\'' +
				", inputData='" + inputData + '\'' +
				", operation_steps='" + operation_steps + '\'' +
				", expectation='" + expectation + '\'' +
				", remark='" + remark + '\'' +
				", priorityId=" + priorityId +
				", updateTime=" + updateTime +
				", executeTime=" + executeTime +
				", flag=" + flag +
				", num=" + num +
				", id=" + id +
				", createTime=" + createTime +
				'}';
	}
}
