package com.scau.model;

//执行测试用例信息
public class Record extends BaseModel {
	private Long userId;// 执行人id
	private Long testcaseId;// 测试用例id
	private Long resultId;// 执行结果id
	private String truth;//实际情况
    private String expectation;//执行结果


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTestcaseId() {
		return testcaseId;
	}

	public void setTestcaseId(Long testcaseId) {
		this.testcaseId = testcaseId;
	}

	public Long getResultId() {
		return resultId;
	}

	public void setResultId(Long resultId) {
		this.resultId = resultId;
	}

	public String getTruth() {
		return truth;
	}

	public void setTruth(String truth) {
		this.truth = truth;
	}

	public String getExpectation() {
		return expectation;
	}

	public void setExpectation(String expectation) {
		this.expectation = expectation;
	}

	@Override
	public String toString() {
		return "Record{" +
				"userId=" + userId +
				", testcaseId=" + testcaseId +
				", resultId=" + resultId +
				", truth='" + truth + '\'' +
				", expectation='" + expectation + '\'' +
				", updateTime=" + updateTime +
				'}';
	}
}
