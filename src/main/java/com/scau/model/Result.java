package com.scau.model;

import org.apache.ibatis.type.Alias;

//执行结果
@Alias("Result")
public class Result extends BaseModel {
    private String resultName;
    private Integer flag;//0表示正常，1表示失效-逻辑删除；

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    @Override
    public String toString() {
        return "Result [resultName=" + resultName + ", id=" + id + ", createTime=" + createTime + ", updateTime=" + updateTime
                + "]";
    }

}
