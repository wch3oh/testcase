package com.scau.model;

import org.apache.ibatis.type.Alias;

@Alias("Browser")
public class Browser extends BaseModel{
    private String browserName;//浏览器
    private Integer flag;//删除标记

    public String getBrowserName() {
        return browserName;
    }

    public void setBrowserName(String browserName) {
        this.browserName = browserName;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Browser{" +
                "browserName='" + browserName + '\'' +
                ", flag=" + flag +
                ", id=" + id +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
