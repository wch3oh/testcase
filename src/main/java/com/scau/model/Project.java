package com.scau.model;

import org.apache.ibatis.type.Alias;

//测试项目(产品)
@Alias("Project")
public class Project extends BaseModel {
	private Long userId;//产品负责人
	private String projectName;// 项目名称
	private String content;// 项目内容
	private Integer flag;//0表示正常，1表示失效-逻辑删除；

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Project{" +
				"userId=" + userId +
				", projectName='" + projectName + '\'' +
				", content='" + content + '\'' +
				", flag=" + flag +
				", id=" + id +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				'}';
	}
}
