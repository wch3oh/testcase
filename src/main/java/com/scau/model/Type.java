package com.scau.model;

import org.apache.ibatis.type.Alias;

//用例类型
@Alias("Type")
public class Type extends BaseModel {
	private String typeName;
    private Integer flag;//0表示正常，1表示失效-逻辑删除；

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@Override
	public String toString() {
		return "Type [typeName=" + typeName + ", id=" + id + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}

}
