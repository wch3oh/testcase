package com.scau.model;

import org.apache.ibatis.type.Alias;

//优先级
@Alias("Priority")
public class Priority extends BaseModel {
	private String priorityName;
	private Integer flag;//0表示正常，1表示失效-逻辑删除；

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public String getPriorityName() {
		return priorityName;
	}

	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}

	@Override
	public String toString() {
		return "Priority [priorityName=" + priorityName + ", id=" + id + ", createTime=" + createTime + ", updateTime="
				+ updateTime + "]";
	}

}
