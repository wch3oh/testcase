package com.scau.model;

import org.apache.ibatis.type.Alias;

//项目模块
@Alias("Module")
public class Module extends BaseModel {

	private Long projectId;
	private String moduleName;
	private Integer flag;//0表示正常，1表示失效-逻辑删除；

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	@Override
	public String toString() {
		return "Module [projectId=" + projectId + ", moduleName=" + moduleName + ", id=" + id + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + "]";
	}

	

}
