package com.scau.model;

import org.apache.ibatis.type.Alias;


//反馈
@Alias("FeedBack")
public class FeedBack extends BaseModel {
    private Long userId;
    private String feedback; //反馈详情
    private String remark;//处理备注
    private Integer flag;//0表示未查看，1表示已查看，2表示已处理，3表示不予处理

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "FeedBack{" +
                "userId=" + userId +
                ", feedback='" + feedback + '\'' +
                ", remark='" + remark + '\'' +
                ", flag=" + flag +
                ", id=" + id +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
