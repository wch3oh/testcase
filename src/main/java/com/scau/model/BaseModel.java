package com.scau.model;


import java.sql.Timestamp;


public abstract class BaseModel {
	protected Long id;//id
	protected Timestamp createTime;//创建时间
	protected Timestamp updateTime;//更新时间


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}


}
