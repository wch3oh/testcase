package com.scau.model;

import org.apache.ibatis.type.Alias;

//用例状态״̬
@Alias("Status")
public class Status extends BaseModel {
	private String statusName;
	private Integer flag;//0表示正常，1表示失效-逻辑删除；

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	@Override
	public String toString() {
		return "Status [statusName=" + statusName + ", id=" + id + ", createTime=" + createTime + ", updateTime=" + updateTime
				+ "]";
	}

}
