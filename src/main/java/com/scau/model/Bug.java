package com.scau.model;

import org.apache.ibatis.type.Alias;

import java.sql.Timestamp;

@Alias("Bug")
public class Bug extends BaseModel {
    private Long requirementId;//所属需求
    private String bugType;//bug类型
    private Long soId;//bug的操作系统
    private Long browserId;//浏览器
    private String title;//标题
    private String operation_steps;//操作步骤
    private Long programmerId;//指派的开发人员id
    private Long userId;//创建bug人员id
    private Long priorityId;//优先级Id
    private Integer flag;//0=打开，1=已确认，2=已解决，3=已关闭
    private String solution;//解决方案
    private Long closeUserId;//关闭bug的人员
    private Timestamp closeTime;//bug关闭的时间
    private Long workUserId;//关闭bug的开发人员id

    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }

    public String getBugType() {
        return bugType;
    }

    public void setBugType(String bugType) {
        this.bugType = bugType;
    }

    public Long getSoId() {
        return soId;
    }

    public void setSoId(Long soId) {
        this.soId = soId;
    }

    public Long getBrowserId() {
        return browserId;
    }

    public void setBrowserId(Long browserId) {
        this.browserId = browserId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOperation_steps() {
        return operation_steps;
    }

    public void setOperation_steps(String operation_steps) {
        this.operation_steps = operation_steps;
    }

    public Long getProgrammerId() {
        return programmerId;
    }

    public void setProgrammerId(Long programmerId) {
        this.programmerId = programmerId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Long priorityId) {
        this.priorityId = priorityId;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public Long getCloseUserId() {
        return closeUserId;
    }

    public void setCloseUserId(Long closeUserId) {
        this.closeUserId = closeUserId;
    }

    public Timestamp getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Timestamp closeTime) {
        this.closeTime = closeTime;
    }

    public Long getWorkUserId() {
        return workUserId;
    }

    public void setWorkUserId(Long workUserId) {
        this.workUserId = workUserId;
    }

    @Override
    public String toString() {
        return "Bug{" +
                "requirementId=" + requirementId +
                ", bugType='" + bugType + '\'' +
                ", soId=" + soId +
                ", browserId=" + browserId +
                ", title='" + title + '\'' +
                ", operation_steps='" + operation_steps + '\'' +
                ", programmerId=" + programmerId +
                ", userId=" + userId +
                ", priorityId=" + priorityId +
                ", flag=" + flag +
                ", solution='" + solution + '\'' +
                ", closeUserId=" + closeUserId +
                ", closeTime=" + closeTime +
                ", workUserId=" + workUserId +
                ", id=" + id +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
