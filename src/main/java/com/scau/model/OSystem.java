package com.scau.model;

import org.apache.ibatis.type.Alias;

@Alias("OSystem")
public class OSystem extends BaseModel{
    private String OperatingSystem;//操作系统
    private Integer flag;//删除标记

    public String getOperatingSystem() {
        return OperatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        OperatingSystem = operatingSystem;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "OSystem{" +
                "OperatingSystem='" + OperatingSystem + '\'' +
                ", flag=" + flag +
                ", id=" + id +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
