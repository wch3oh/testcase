package com.scau.model;

import org.apache.ibatis.type.Alias;

//适用阶段
@Alias("Stage")
public class Stage extends BaseModel {
	private String stageName;
	private Integer flag;//0表示正常，1表示失效-逻辑删除；

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	@Override
	public String toString() {
		return "Stage [stageName=" + stageName + ", id=" + id + ", createTime=" + createTime + ", updateTime=" + updateTime
				+ "]";
	}

}
