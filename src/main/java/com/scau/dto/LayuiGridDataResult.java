package com.scau.dto;

import java.util.List;

/**
 * layui表格数据
 */
public class LayuiGridDataResult {
    private Integer code;
    private List<?> data;
    private Long count;
    private String msg;

    public LayuiGridDataResult() {
        this.code=0;
        this.msg="";
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }
}
