package com.scau.dto;

/**
 * 返回结果
 */
public class ResultModel {

    private Integer code;

    private String msg;//返回的结果信息

    private Object data;//返回的结果类型

    public ResultModel(Integer code,String msg){
        this.msg=msg;
        this.code=code;
    }

    public ResultModel(Integer code,String msg,Object data){
        this.code=code;
        this.msg=msg;
        this.data=data;
    }
    public ResultModel(Integer code,Object data){
        this.code=code;
        this.msg="";
        this.data=data;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
