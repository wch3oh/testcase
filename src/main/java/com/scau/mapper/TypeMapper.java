package com.scau.mapper;

import java.util.List;

import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.Type;

//用例类型
@Mapper
public interface TypeMapper {
	/**
	 * 添加类型
	 * @param type
	 */
	int insert(Type type);

	/**
	 * 插入并返回id
	 * @param type
	 * @return
	 */
	Long insertSelective(Type type);
	
	/**
	 * 根据id删除类型(修改flag)
	 * @param type
	 */
	int delete(Type type);
	
	/**
	 * 更新类型
	 * @param type
	 */
	int update(Type type);

	/**
	 * 获取所有类型
	 * @return
	 */
	List<Type> list();

	/**
	 * 选出前9个类型
	 * @return
	 */
	List<Type> selectByLimit();

	/**
	 * 通过id查找类型
	 * @param id
	 * @return
	 */
	Type selectByTypeId(Long id);
	
	/**
	 * 同过类型名查找（模糊搜索）
	 * @param typeName
	 * @return
	 */
	List<Type> listByTypeName(String typeName);

	Type selectByTypeName(String typeName);

	List<SelectVo> selectAll();
}
