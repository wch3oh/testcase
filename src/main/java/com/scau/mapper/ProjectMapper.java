package com.scau.mapper;

import java.util.List;

import com.scau.vo.ProjectVo;
import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.Project;


//项目
@Mapper
public interface ProjectMapper {
	/**
	 * 添加
	 * @param project
	 */
	int insert(Project project);

	/**
	 * 插入并返回id
	 * @param project
	 * @return
	 */
	Long insertSelective(Project project);
	
	/**
	 * 根据id删除
	 * @param id
	 */
	int delete(Long id);
	/**
	 * 修改项目
	 * @param project
	 * @return
	 */
	int update(Project project);


	/**
	 * 通过id查找
	 * @param id
	 * @return
	 */
	Project selectByProjectId(Long id);
	
	/**
	 * 通过名称查找
	 * @param projectName
	 * @return
	 */
	List<ProjectVo> list(@Param("projectName") String projectName, @Param("userId") Long userId);
    Project selectByName(String projectName);
	List<SelectVo> selectAll();
}
