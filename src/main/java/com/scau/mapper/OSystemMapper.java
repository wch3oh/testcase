package com.scau.mapper;

import com.scau.model.OSystem;
import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

//操作系统
@Mapper
public interface OSystemMapper {
    /**
     * 添加
     *
     * @param osystem
     */
    int insert(OSystem osystem);

    /**
     * 插入并返回id
     *
     * @param osystem
     * @return
     */
    Long insertSelective(OSystem osystem);

    /**
     * 删除
     * @param osystem
     */
    int delete(OSystem osystem);

    /**
     * 更新
     *
     * @param osystem
     */
    int update(OSystem osystem);

    /**
     * 获取所有操作系统
     *
     * @return
     */
    List<OSystem> list();

    /**
     * 通过id查找
     *
     * @param id
     * @return
     */
    OSystem selectByOSystemId(Long id);

    /**
     * 通过名称查找(模糊搜索）
     *
     * @param operatingSystem
     * @return
     */
    List<OSystem> listByName(String operatingSystem);

    OSystem selectByName(String operatingSystem);

    List<SelectVo> selectAll();
}
