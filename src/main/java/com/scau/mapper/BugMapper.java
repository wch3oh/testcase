package com.scau.mapper;

import com.scau.model.Bug;
import com.scau.vo.BugVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BugMapper {
    /**
     * 添加Bug(flag默认为0=打开）
     *
     * @param bug
     */
    int insert(Bug bug);

    /**
     * 更新bug信息
     *
     * @param Bug
     * @return
     */
    int update(Bug Bug);

    /**
     * 更新flag
     *
     * @param Bug
     * @return
     */
    int updateFlag(Bug Bug);

    /**
     * 通过测试用例id查找测试用例
     *
     * @param bugId
     * @return
     */
    Bug selectByBugId(Long bugId);
    BugVo getByBugId(Long bugId);

    /**
     * 根据创建用户id查找
     * @return
     */
    List<BugVo> listByUserId(@Param("title") String title, @Param("userId")Long userId, @Param("flag") Integer flag);
    List<BugVo> listByProgrammerId(@Param("title") String title,@Param("porgrammerId")Long programmerId);
    List<BugVo> listByWorkUserId(@Param("title") String title,@Param("workUserId")Long workUserId);
    List<BugVo> listByCloseUserId(@Param("title") String title,@Param("closeUserId")Long closeUserId);

    /**
     * 根据flag查找
     * @return
     */
    List<BugVo> listByFlag(@Param("title") String title,@Param("flag") Integer flag);
    List<Bug> selectByFlag(@Param("title") String title,@Param("flag")Integer flag);

    /**
     * 根据优先级id查找
     * @return
     */
    List<BugVo> listByPriorityId(@Param("title") String title, @Param("priorityId")Long priorityId, @Param("flag") Integer flag);

    /**
     * 根据需求id查找
     * @return
     */
    List<BugVo> listByRequirementId(@Param("title") String title, @Param("requirementId")Long requirementId, @Param("flag") Integer flag);

    /**
     * 查找所有bug
     * @param flag
     * @return
     */
    List<BugVo> list(Integer flag);
}
