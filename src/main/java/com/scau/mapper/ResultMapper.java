package com.scau.mapper;

import java.util.List;

import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.Result;

//执行结果
@Mapper
public interface ResultMapper {
	/**
	 * 添加
	 * @param result
	 */
	int insert(Result result);

	/**
	 * 插入并返回id
	 * @param result
	 * @return
	 */
	Long insertSelective(Result result);
	
	/**
	 * 根据id删除
	 * @param id
	 */
	int delete(Long id);
	
	/**
	 * 更新
	 * @param result
	 */
	int update(Result result);

	/**
	 * 获取所有执行结果
	 * @return
	 */
	List<Result> list();


	/**
	 * 通过id查找
	 * @param id
	 * @return
	 */
	Result selectByResultId(Long id);
	
	/**
	 * 通过名称查找（模糊搜索）
	 * @param resultName
	 * @return
	 */
	List<Result> listByName(String resultName);
	Result selectByName(String resultName);

	List<SelectVo> selectAll();
}
