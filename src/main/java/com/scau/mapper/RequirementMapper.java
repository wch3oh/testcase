package com.scau.mapper;

import java.util.List;

import com.scau.vo.RequirementVo;
import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.Requirement;

//需求
@Mapper
public interface RequirementMapper {
	/**
	 * 增加需求
	 * @param requirement
	 */
	int insert(Requirement requirement);
	
	/**
	 * 删除需求
	 * @param id
	 */
	int delete(Long id);
	
	/**
	 * 更新需求
	 * @param requirement
	 */
	int update(Requirement requirement);
	
	/**
	 * 通过id查找需求
	 * @return
	 */
	Requirement selectByRequirementId(Long requirementId);
	RequirementVo selectById(Long requirementId);

	/**
	 * 通过需求名查找模块(模糊搜索)
	 * @param requirementName
	 * @return
	 */
	List<RequirementVo> list(@Param("testFlag")Integer testFlag,
												@Param("projectId")Long projectId,
												@Param("moduleId")Long moduleId,
	                                            @Param("requirementName") String requirementName,
												@Param("testUserId")Long testUserId,
												@Param("userId")Long userId);
	Requirement selectByName(@Param("moduleId")Long moduleId,@Param("requirementName")String requirementName);
	/**
	 * 列出所有需求
	 * @return
	 */
    List<RequirementVo> listByProjectUserId(Long userId);
	/**
	 * 列出某个moduleId下的所有需求
	 * @param moduleId
	 * @return
	 */
	List<RequirementVo> selectByModuleId(Long moduleId);

	List<SelectVo> selectAll();
}
