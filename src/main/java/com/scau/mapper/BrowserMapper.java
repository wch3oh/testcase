package com.scau.mapper;

import com.scau.model.Browser;
import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BrowserMapper {
    /**
     * 添加
     * @param browser
     */
    int insert(Browser browser);

    /**
     * 插入并返回id
     * @param browser
     * @return
     */
    Long insertSelective(Browser browser);

    /**
     * 删除
     * @param browser
     */
    int delete(Browser browser);

    /**
     * 更新
     * @param browser
     */
    int update(Browser browser);

    /**
     * 获取所有优先级
     * @return
     */
    List<Browser> list();

    /**
     * 通过id查找
     * @param id
     * @return
     */
    Browser selectByBrowserId(Long id);

    /**
     * 通过名称查找(模糊搜索）
     * @param browserName
     * @return
     */
    List<Browser> listByName(String browserName);
    Browser selectByName(String browserName);

    List<SelectVo> selectAll();
}
