package com.scau.mapper;
import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.User;

import java.util.List;

//用户账号信息
@Mapper
public interface UserMapper {

	/**
	 * 增加用户
	 * @param user
	 */
  Long insert(User user);
	
  /**
   * 根据id删除用户
   * @param id
   */
  int delete(Long id);
	
	/**
	 * 修改密码
	 * @param user
	 */
  int updatePassword(User user);
	
	/**
	 * 修改用户信息
	 * @param user
	 */
  int update(User user);

	/**
	 * 通过id获取用户
	 * @param id
	 * @return
	 */
	User selectByUserId(Long id);
	
	/**
	 * 查找所有用户
	 * @return
	 */
	List<User> list(@Param("name")String name,@Param("sex")String sex,@Param("authorityFlag")Integer authorityFlag);
	
	/**
	 * 通过账户密码获取用户
	 * @param account
	 * @param password
	 * @return
	 */
	User selectByAccountAndPassword(@Param("account")String account,@Param("password")String password);
	
	/**
	 * 通过账号查询用户
	 * @param account
	 * @return
	 */
	User selectByAccount(String account);

    List<SelectVo> selectAll();

}
