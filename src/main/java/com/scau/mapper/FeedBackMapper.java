package com.scau.mapper;

import com.scau.model.FeedBack;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FeedBackMapper {
    /**
     * 添加反馈
     * @param feedback
     * @return
     */
    int insert(FeedBack feedback);

    /**
     * 插入并返回id
     * @param feedback
     * @return
     */
    Long insertSelective(FeedBack feedback);

    /**
     * 根据id删除
     * @param id
     */
    int delete(Long id);

    /**
     * 修改
     * @param feedback
     * @return
     */
    int update(FeedBack feedback);

    /**
     * 获取所有反馈
     * @return
     */
    List<FeedBack> list(@Param("userId")Long userId,@Param("flag")Integer flag);

    /**
     * 通过id查找
     * @param id
     * @return
     */
    FeedBack selectById(Long id);
}
