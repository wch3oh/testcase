package com.scau.mapper;

import java.util.List;

import com.scau.vo.RecordVo;
import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.Record;

//执行记录
@Mapper
public interface RecordMapper {

	/**
	 * 添加执行记录
	 * @param record
	 */
	int insert(Record record);
	
	/**
	 * 插入执行记录并返回id
	 * @param record
	 * @return
	 */
	Long insertSelective(Record record);
	

	/**
	 * 根据执行记录id查找
	 * @param id
	 * @return
	 */
	Record selectByRecordId(Long id);
	

	/**
	 * 通过测试用例id查找执行记录
	 * @param testcaseId
	 * @return
	 */
	//List<Record> selectByTestCaseId(Long testcaseId);
    List<RecordVo> selectByTestCaseId(Long testcaseId);

	List<SelectVo> selectAll();
}
