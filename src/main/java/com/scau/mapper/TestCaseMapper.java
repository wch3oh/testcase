package com.scau.mapper;

import java.util.List;

import com.scau.vo.TestCaseVo;
import jdk.nashorn.internal.ir.IdentNode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.TestCase;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.criteria.CriteriaBuilder;

//测试用例
@Mapper
public interface TestCaseMapper {
	/**
	 * 添加测试用例(flag默认为0）
	 * 
	 * @param testcase
	 */
	int insert(TestCase testcase);

	/**
	 * 更新测试用例信息，flag=0
	 * 
	 * @param testcase
	 * @return
	 */
	int update(TestCase testcase);

	/**
	 * 更新flag
	 * 
	 * @param testcase
	 * @return
	 */
	int updateFlag(TestCase testcase);
	
	/**
	 * 删除测试用例（回收站中彻底删除）
	 * @param id
	 * @return
	 */
	int delete(Long id);
	
	/**
	 * 通过测试用例id查找测试用例
	 * 
	 * @param testcaseId
	 * @return
	 */
	TestCase selectByTestcaseId(Long testcaseId);
	TestCaseVo getByTestcaseId(Long testcseId);

	/**
	 * 通过title查找
	 * @return
	 */
	List<TestCase> selectByTitle(@Param("title") String title,@Param("flag") Integer flag);
	List<TestCaseVo> listByTitle(@Param("title") String title,@Param("flag") Integer flag);
	/**
	 * 根据用户id查找
	 * @return
	 */
	List<TestCaseVo> listByUserId(@Param("title") String title, @Param("userId")Long userId, @Param("flag") Integer flag);

	
	/**
	 * 根据类型id查找
	 * @return
	 */
	List<TestCaseVo> listByTypeId(@Param("title") String title, @Param("typeId")Long typeId, @Param("flag") Integer flag);

	
	/**
	 * 根据优先级id查找
	 * @return
	 */
	List<TestCaseVo> listByPriorityId(@Param("title") String title, @Param("priorityId")Long priorityId, @Param("flag") Integer flag);
	
	/**
	 * 根据适用阶段id查找
	 * @return
	 */
	List<TestCaseVo> listByStageId(@Param("title") String title, @Param("stageId")Long stageId, @Param("flag") Integer flag);

	
	/**
	 * 根据测试用例状态查找
	 * @return
	 */
	List<TestCaseVo> listByStatusId(@Param("title") String title, @Param("statusId")Long statusId, @Param("flag") Integer flag);

	
	/**
	 * 根据执行结果查找
	 * @return
	 */
	List<TestCaseVo> listByResultId(@Param("title") String title, @Param("resultId")Long resultId, @Param("flag") Integer flag);
	
	/**
	 * 根据需求id查找
	 * @return
	 */
	List<TestCaseVo> listByRequirementId(@Param("title") String title, @Param("requirementId")Long requirementId, @Param("flag") Integer flag);
	
	/**
	 * 根据模块id查找
	 * @return
	 */
	List<TestCaseVo> listByModuleId(@Param("title") String title, @Param("moduleId")Long moduleId, @Param("flag") Integer flag);

	
	/**
	 * 根据产品id查找
	 * @return
	 */
	List<TestCaseVo> listByProjectId(@Param("title") String title, @Param("projectId")Long projectId, @Param("flag") Integer flag);

	
	/**
	 * 查找所有测试用例
	 * @param flag
	 * @return
	 */
	List<TestCaseVo> list(Integer flag);

}
