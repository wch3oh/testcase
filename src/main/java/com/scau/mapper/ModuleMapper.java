package com.scau.mapper;


import java.util.List;

import com.scau.vo.ModuleVo;
import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.Module;
import org.apache.ibatis.session.RowBounds;

//项目模块
@Mapper
public interface ModuleMapper {
	/**
	 * 增加项目模块
	 * @param module
	 */
	int insert(Module module);
	
	/**
	 * 删除项目模块
	 * @param id
	 */
	int delete(Long id);
	
	/**
	 * 更新模块
	 * @param module
	 */
	int update(Module module);
	
	/**
	 * 通过id查找模块
	 * @param id
	 * @return
	 */
	Module selectByModuleId(Long id);
	
	/**
	 * 通过模块名查找模块
	 * @param moduleName
	 * @return
	 */
	Module selectByModuleName(@Param("moduleName") String moduleName,@Param("projectId") Long projectId);
	/**
	 * 根据产品id获取
	 * @param projectId
	 * @return
	 */
	List<Module> listByProjectId(Long projectId);

	/**
	 * 搜索
	 * @param projectId
	 * @param moduleName
	 * @param userId
	 * @return
	 */
	List<ModuleVo> list(@Param("projectId") Long projectId,@Param("moduleName")String moduleName,@Param("userId")Long userId);


	List<SelectVo> selectAll();
}
