package com.scau.mapper;


import java.util.List;

import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Param;

import com.scau.model.Priority;

import org.apache.ibatis.annotations.Mapper;


//优先级
@Mapper
public interface PriorityMapper {
	/**
	 * 添加
	 * @param priority
	 */
	int insert(Priority priority);

	/**
	 * 插入并返回id
	 * @param priority
	 * @return
	 */
	Long insertSelective(Priority priority);
	
	/**
	 * 根据id删除
	 * @param id
	 */
	int delete(Long id);
	
	/**
	 * 更新
	 * @param priority
	 */
	int update(Priority priority);

	/**
	 * 获取所有优先级
	 * @return
	 */
	List<Priority> list();

	/**
	 * 通过id查找
	 * @param id
	 * @return
	 */
	Priority selectByPriorityId(Long id);
	
	/**
	 * 通过名称查找(模糊搜索）
	 * @param priorityName
	 * @return
	 */
	List<Priority> listByName(String priorityName);
	Priority selectByName(String priorityName);

	List<SelectVo> selectAll();
}
