package com.scau.mapper;

import java.util.List;

import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.Stage;

//适用阶段
@Mapper
public interface StageMapper {
	
	/**
	 * 添加适用阶段
	 * 
	 * @param stage
	 */
	int insert(Stage stage);

	/**
	 * 插入并返回id
	 * 
	 * @param stage
	 * @return
	 */
	Long insertSelective(Stage stage);

	/**
	 * 根据id删除类型
	 * 
	 * @param id
	 */
	int delete(Long id);

	/**
	 * 更新适用阶段
	 * 
	 * @param stage
	 */
	int update(Stage stage);

	/**
	 * 获取所有适用阶段
	 * 
	 * @return
	 */
	List<Stage> list();

	/**
	 * 通过id查找stage
	 * 
	 * @param id
	 * @return
	 */
	Stage selectByStageId(Long id);

	/**
	 * 通过适用阶段名称查找（模糊搜索）
	 * 
	 * @param stageName
	 * @return
	 */
	List<Stage> listByName(String stageName);
    Stage selectByName(String stageName);

	List<SelectVo> selectAll();
}
