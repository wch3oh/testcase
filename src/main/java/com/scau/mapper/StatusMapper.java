package com.scau.mapper;

import java.util.List;

import com.scau.vo.SelectVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.scau.model.Status;


//用例状态
@Mapper
public interface StatusMapper {
	/**
	 * 添加用例状态
	 * 
	 * @param status
	 */
	int insert(Status status);

	/**
	 * 插入并返回id
	 * 
	 * @param status
	 * @return
	 */
	Long insertSelective(Status status);

	/**
	 * 根据id删除用例状态
	 * 
	 * @param id
	 */
	int delete(Long id);

	/**
	 * 更新用例状态
	 * 
	 * @param status
	 */
	int update(Status status);

	/**
	 * 获取所有用例状态
	 * 
	 * @return
	 */
	List<Status> list();

	/**
	 * 通过id查找stage
	 * 
	 * @param id
	 * @return
	 */
	Status selectByStatusId(Long id);

	/**
	 * 通过用例状态名查找（模糊搜索）
	 * 
	 * @param statusName
	 * @return
	 */
	List<Status> listByName(String statusName);
    Status selectByName(String statusName);

	List<SelectVo> selectAll();
}