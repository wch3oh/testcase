package com.scau.controller;

import java.util.List;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scau.model.Type;
import com.scau.service.TypeService;


@Controller
@RequestMapping("/type")
public class TypeController extends BaseController {

    @Autowired
    private TypeService typeService;

    /**
     * 添加测试用例类型
     *
     * @param typeName
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel add(@RequestParam("typeName") String typeName) {
        if (typeService.selectByTypeName(typeName) == null) {
                Type type = new Type();
                type.setTypeName(typeName);
                long id = typeService.add(type);
                if (id > 0) {
                    type.setId(id);
                }
                return new ResultModel(Constants.SUCCESS, "添加成功", type);
            } else {
            return new ResultModel(Constants.FAIL, "添加失败,已存在该类型");
        }

    }

    /**
     * 修改类型名
     *
     * @param typeName
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel update(@RequestParam("typeId") Long typeId,
                              @RequestParam("typeName") String typeName) {
        boolean result = false;
        Type type = typeService.get(typeId);
        if (type != null) {//该类型存在
            type.setTypeName(typeName);
            if (typeService.update(type) == 1) {
                return new ResultModel(Constants.SUCCESS, "修改成功");
            }
        }
        return new ResultModel(Constants.FAIL, "修改失败");
    }

    /**
     * 删除类型
     *
     * @param typeIds
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel delete(@RequestParam("ids") String typeIds) {
        if (typeIds != null) {
            List<Long> idList = ArrayUtil.toList(typeIds);
            for (Long typeId : idList) {
                Type type = typeService.get(typeId);
                typeService.delete(type);
            }
            return new ResultModel(Constants.SUCCESS, "删除成功");
        }
        return new ResultModel(Constants.FAIL, "删除失败");
    }

    /**
     * 获取所有类型
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult list(int limit, int page) {
        startPage(page, limit);
        List<Type> typeList = typeService.listAll();
        return getDataTable(typeList);
    }

    /**
     * 根据类型名获取（模糊搜索）
     *
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult search(int limit, int page, @RequestParam("typeName") String typeName) {
        startPage(page, limit);
        List<Type> typeList = typeService.search(typeName);
        return getDataTable(typeList);
    }

    /**
     * 根据id获取
     *
     * @param typeId
     * @return
     */
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public Type get(@RequestParam("typeId") Long typeId) {
        return typeService.get(typeId);
    }
}
