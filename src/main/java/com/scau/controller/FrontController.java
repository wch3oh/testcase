package com.scau.controller;

import com.scau.dto.ResultModel;
import com.scau.service.*;
import com.scau.util.Constants;
import com.scau.vo.SelectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/front")
public class FrontController {
    @Autowired
    private ResultService resultService;
    @Autowired
    private PriorityService priorityService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ModuleService moduleService;
    @Autowired
    private RequirementService requirementService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private StatusService statusService;
    @Autowired
    private StageService stageService;
    @Autowired
    private UserService userService;

    /**
     * 获取数据库中的有关表信息（仅获取id和name)
     */
    @RequestMapping("/select")
    @ResponseBody
    public ResultModel get(@RequestParam("typeId")int typeId){
        List<SelectVo> list=new ArrayList<>();
        switch (typeId){
            case Constants.USER:{
                list=userService.selectAll();
                break;
            }
            case Constants.PRIORITY:{
                list=priorityService.selectAll();
                break;
            }
            case Constants.PROJECT:{
                list=projectService.selectAll();
                break;
            }
            case Constants.MODULE:{
                list=moduleService.selectAll();
                break;
            }
            case Constants.REQUIREMENT:{
                list=requirementService.selectAll();
                break;
            }
            case Constants.RESULT:{
                list=resultService.selectAll();
                break;
            }
            case Constants.STAGE:{
                list=stageService.selectAll();
                break;
            }
            case Constants.STATUS:{
                list=statusService.selectAll();
                break;
            }
            case Constants.TESCASE:{
                list=typeService.selectAll();
                break;
            }
        }
        return new ResultModel(Constants.SUCCESS,"",list);
    }

}
