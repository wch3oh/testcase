package com.scau.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.scau.dto.LayuiGridDataResult;

import java.util.List;

public class BaseController {
    protected void startPage(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected LayuiGridDataResult getDataTable(List<?> list) {
        LayuiGridDataResult result=new LayuiGridDataResult();
        result.setData(list);
        result.setCount(new PageInfo(list).getTotal());
        return result;
    }
}
