package com.scau.controller;

import java.util.List;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.model.Result;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.scau.model.Priority;
import com.scau.service.PriorityService;

@Controller
@RequestMapping("/priority")
public class PriorityController extends BaseController{

	@Autowired
	private PriorityService priorityService;
	
	/**
	 * 添加优先级
	 * @param priorityName
	 * @return
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel add(@RequestParam("priorityName")String priorityName){
		Priority priority=null;
		if(priorityService.selectByName(priorityName)==null){
				priority=new Priority();
				priority.setPriorityName(priorityName);
				long id=priorityService.add(priority);
				if(id>0){
					priority.setId(id);
				}
				return new ResultModel(Constants.SUCCESS,"添加成功",priority);
			}
		return new ResultModel(Constants.FAIL,"添加失败,已存在该优先级");
	}
	
	/**
	 * 修改优先级信息
	 * @param priorityId
	 * @param priorityName
	 * @return
	 */
	@RequestMapping(value="/update",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel update(@RequestParam("priorityId")Long priorityId,@RequestParam("priorityName")String priorityName){
		Priority priority=priorityService.get(priorityId);
		if(priority!=null){
				priority.setPriorityName(priorityName);
				if(priorityService.update(priority)==1){
					return new ResultModel(Constants.SUCCESS,"修改成功",priority);
				}
			}
		return new ResultModel(Constants.SUCCESS,"修改失败");
	}
	
	/**
	 * 删除优先级
	 * @param priorityIds
	 * @return
	 */
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel delete(@RequestParam("ids")String priorityIds ) {
		if(priorityIds!=null) {
			List<Long> list=ArrayUtil.toList(priorityIds);
			for(Long priorityId:list){
				Priority priority=priorityService.get(priorityId);
				if(priority!=null) {//优先级存在
					if(priorityService.delete(priorityId)==1){
						return new ResultModel(Constants.FAIL,"删除失败");
					}
				}
				return new ResultModel(Constants.SUCCESS,"删除成功");
			}
		}
		return new ResultModel(Constants.SUCCESS,"删除失败");
	}
	
	/**
	 * 获取所有优先级
	 * @return
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult list(int limit, int page){
		startPage(page,limit);
		List<Priority> priorityList=priorityService.listAll();
		return getDataTable(priorityList);
	}

	/**
	 * 根据名称获取（模糊搜索）
	 * @return
	 */
	@RequestMapping(value="/search",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult search(int limit,int page,@RequestParam("priorityName")String priorityName){
		startPage(page,limit);
		List<Priority> list=priorityService.search(priorityName);
		return getDataTable(list);
	}

	/**
	 * 根据id获取
	 * @param priorityId
	 * @return
	 */
	@RequestMapping(value="/get",method=RequestMethod.GET)
	@ResponseBody
	public Priority get(@RequestParam("priorityId")Long priorityId){
		return priorityService.get(priorityId);
	}
}

