package com.scau.controller;

import java.util.ArrayList;
import java.util.List;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.model.Result;
import com.scau.model.User;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import com.scau.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scau.model.Module;
import com.scau.service.ModuleService;
import com.scau.service.ProjectService;
import com.scau.vo.ModuleVo;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/module")
public class ModuleController extends BaseController {

	@Autowired
	private ModuleService moduleService;

	/**
	 * 添加模块
	 * @return
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel add(Module module){
		if(module.getId()==null){//添加
			if(moduleService.selectByName(module.getModuleName(),module.getProjectId())==null){
				if(moduleService.add(module)>0){
					return new ResultModel(Constants.SUCCESS,"添加成功",module);
				}
			}else{
				return new ResultModel(Constants.FAIL,"添加失败，该产品下已有同名模块");
			}
			return new ResultModel(Constants.FAIL,"添加失败");
		}else{//编辑
			if(moduleService.update(module)>0){
				return new ResultModel(Constants.SUCCESS,"修改成功",module);
			}
			return new ResultModel(Constants.FAIL,"修改失败");
		}
	}

	/**
	 * 根据id删除
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResultModel delete(@RequestParam("ids") String moduleIds) {
		if (moduleIds != null) {
			List<Long> idList = ArrayUtil.toList(moduleIds);
			for(long moduleId:idList){
				Module module = moduleService.get(moduleId);
				if (module != null) {// 需求存在
					if (moduleService.delete(moduleId) != 1) {
						return new ResultModel(Constants.FAIL, "删除失败");
					}
				}
			}
			return new ResultModel(Constants.SUCCESS,"删除成功");
		}
		return new ResultModel(Constants.SUCCESS, "删除失败");
	}

	/**
	 * 根据projectId获取模块列表
	 * @param projectId
	 * @return
	 */
	@RequestMapping(value="/listByProjectId",method=RequestMethod.GET)
	@ResponseBody
	public ResultModel listByProjectId(@RequestParam("projectId")Long projectId){
		List<Module> moduleList=new ArrayList<>();
		if(projectId!=null){
			moduleList=moduleService.listByProjectId(projectId);
		}
		return new ResultModel(Constants.SUCCESS,moduleList);
	}


	/**
	 * 根据参数查询
	 * @param moduleName
	 * @param projectId
	 * @param userId
	 * @return
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult list(int limit,int page,@RequestParam(value = "moduleName",required = false)String moduleName,
								 @RequestParam(value = "projectId",required = false)Long projectId,
									  @RequestParam(value = "userId",required = false)Long userId){
		startPage(page,limit);
		List<ModuleVo> moduleVoList=moduleService.list(projectId,moduleName,userId);
		return getDataTable(moduleVoList);

	}

	/**
	 * 根据id获取
	 * @param moduleId
	 * @return
	 */
	@RequestMapping(value ="/get",method = RequestMethod.GET)
	@ResponseBody
	public ResultModel get(@RequestParam("moduleId")Long moduleId){
		Module module=moduleService.get(moduleId);
		return new ResultModel(Constants.SUCCESS,module);
	}

}

