package com.scau.controller;

import java.util.ArrayList;
import java.util.List;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.model.*;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import com.scau.util.SessionUtil;
import org.omg.PortableInterceptor.RequestInfo;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.scau.service.ModuleService;
import com.scau.service.ProjectService;
import com.scau.service.RequirementService;
import com.scau.vo.RequirementVo;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/requirement")
public class RequirementController extends BaseController {
	@Autowired
	private RequirementService requirementService;

	/**
	 * 添加/编辑需求
	 *
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public ResultModel add(Requirement requirement) {
		if(requirement.getId()==null){//添加
			if(requirementService.selectByName(requirement.getModuleId(),requirement.getRequirementName())==null){
				if(requirementService.add(requirement)>0){
					return new ResultModel(Constants.SUCCESS,"添加成功",requirement);
				}
			}else{
				return new ResultModel(Constants.FAIL,"添加失败，已存在该需求");
			}
			return new ResultModel(Constants.FAIL,"添加失败");
		}else{//编辑
			if(requirementService.update(requirement)>0){
				return new ResultModel(Constants.SUCCESS,"修改成功",requirement);
			}
			return new ResultModel(Constants.FAIL,"修改失败");
		}
	}


	/**
	 * 根据id删除
	 *
	 * @param requirementIds
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResultModel delete(@RequestParam("ids") String requirementIds) {
		if (requirementIds != null) {
			List<Long> idList = ArrayUtil.toList(requirementIds);
			for(long requirementId:idList){
				Requirement requirement = requirementService.get(requirementId);
				if (requirement != null) {// 需求存在
					if (requirementService.delete(requirementId) != 1) {
						return new ResultModel(Constants.FAIL, "删除失败");
					}
				}
			}
			return new ResultModel(Constants.SUCCESS,"删除成功");
		}
		return new ResultModel(Constants.SUCCESS, "删除失败");
	}

	/**
	 * 通过id获取
	 * @param requirementId
	 * @return
	 */
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	@ResponseBody
	public ResultModel get(@RequestParam("requirementId") Long requirementId) {
		RequirementVo requirementVo=requirementService.selectById(requirementId);
		return new ResultModel(Constants.SUCCESS,requirementVo);

	}


	/**
	 * 根据模块id获取
	 * @param moduleId
	 * @return
	 */
	@RequestMapping(value = "/selectByModuleId", method = RequestMethod.GET)
	@ResponseBody
	public ResultModel selectByModuleId(@RequestParam("moduleId") Long moduleId) {
		List<RequirementVo> requirementVoList=requirementService.selectByModuleId(moduleId);
		return  new ResultModel(Constants.SUCCESS,requirementVoList);
	}

	/**
	 * 根据参数查询
	 * @param testFlag
	 * @param requirementName
	 * @param moduleId
	 * @param projectId
	 * @param testUserId
	 * @param userId
	 * @return
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult list(int limit,int page,@RequestParam(value = "testFlag",required = false)Integer testFlag,
													 @RequestParam(value = "requirementName",required = false)String requirementName,
													 @RequestParam(value = "moduleId",required = false)Long moduleId,
													 @RequestParam(value = "projectId",required = false)Long projectId,
									                 @RequestParam(value = "testUserId",required = false)Long testUserId,
	                                                 @RequestParam(value = "userId",required = false)Long userId){
		startPage(page,limit);
		List<RequirementVo> requirementVoList=requirementService.list(testFlag,projectId,moduleId,requirementName,testUserId,userId);
		return getDataTable(requirementVoList);

	}

}

