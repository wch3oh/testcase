package com.scau.controller;

import java.util.ArrayList;
import java.util.List;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.model.User;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import com.scau.util.SessionUtil;
import com.scau.vo.ProjectVo;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.scau.model.Project;
import com.scau.service.ProjectService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/project")
public class ProjectController extends BaseController{
	@Autowired
	private ProjectService projectService;
	@Autowired
	private HttpServletRequest request;
	/**
	 * 添加产品/修改产品
	 * @return
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel add(Project project){
		if(project.getId()==null){//添加
			if(projectService.selectByName(project.getProjectName())==null){
				if(projectService.add(project)>0){
					return new ResultModel(Constants.SUCCESS,"添加成功",project);
				}
			}else{
				return new ResultModel(Constants.FAIL,"添加失败，该产品已存在");
			}
			return new ResultModel(Constants.FAIL,"添加失败");
		}else{//编辑
			if(projectService.update(project)>0){
				return new ResultModel(Constants.SUCCESS,"修改成功",project);
			}
			return new ResultModel(Constants.FAIL,"修改失败");
		}
	}

	/**
	 * 根据id删除
	 *
	 * @param projectIds
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResultModel delete(@RequestParam("ids") String projectIds) {
		if (projectIds != null) {
			List<Long> idList = ArrayUtil.toList(projectIds);
			for(long projectId:idList){
				Project project = projectService.get(projectId);
				if (project != null) {// 需求存在
					if (projectService.delete(projectId) != 1) {
						return new ResultModel(Constants.FAIL, "删除失败");
					}
				}
			}
			return new ResultModel(Constants.SUCCESS,"删除成功");
		}
		return new ResultModel(Constants.SUCCESS, "删除失败");
	}

	/**
	 * 根据id获取
	 * @param projectId
	 * @return
	 */
	@RequestMapping(value = "/get",method = RequestMethod.GET)
	@ResponseBody
	public ResultModel get(@RequestParam("projectId")Long projectId){
		Project project=projectService.get(projectId);
		return new ResultModel(Constants.SUCCESS,project);
	}


	/**
	 * 根据参数查询
	 * @param projectName
	 * @param userId
	 * @return
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult list(int limit, int page,
									  @RequestParam(value = "projectName",required = false)String projectName,
									  @RequestParam(value = "userId",required = false)Long userId){
		startPage(page,limit);
		List<ProjectVo> projectVoList=projectService.list(projectName,userId);
		return getDataTable(projectVoList);
	}


}
