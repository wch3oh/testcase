package com.scau.controller;

import com.scau.dto.ResultModel;
import com.scau.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


@ControllerAdvice
public class MyAdviceController {

    private Logger logger=LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResultModel handler(Exception e){
        logger.error(e.getLocalizedMessage());
        return new ResultModel(Constants.FAIL,e.getMessage());
    }
}
