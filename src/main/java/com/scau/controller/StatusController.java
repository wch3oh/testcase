package com.scau.controller;

import java.util.List;
import com.scau.util.ArrayUtil;
import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.scau.model.Status;
import com.scau.service.StatusService;

@Controller
@RequestMapping("/status")
public class StatusController extends BaseController {

	@Autowired
	private StatusService statusService;
	
	/**
	 * 添加状态
	 * @param statusName
	 * @return
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel add(@RequestParam("statusName") String statusName){
		Status status=new Status();
		if(statusService.selectByName(statusName)==null){
			status.setStatusName(statusName);
			long id=statusService.add(status);
			if(id>0)
				status.setId(id);
			return new ResultModel(Constants.SUCCESS,"添加成功",status);
		}
			return new ResultModel(Constants.SUCCESS,"添加失败,已存在该状态");


	}
	
	/**
	 * 更新状态信息
	 * @param statusId
	 * @param statusName
	 * @return
	 */
	@RequestMapping(value="/update",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel update(@RequestParam("statusId")Long statusId, @RequestParam("statusName")String statusName){
		boolean result=false;
		Status status=statusService.get(statusId);
		if(status!=null){
			status.setStatusName(statusName);
			if(statusService.update(status)==1)
				return new ResultModel(Constants.SUCCESS, "修改成功",status);
		}
		return new ResultModel(Constants.SUCCESS, "修改失败");
	}
	
	/**
	 * 根据id删除
	 * @param statusIds
	 * @return
	 */
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel delete(@RequestParam("ids")String statusIds ) {
		if (statusIds != null) {
			List<Long> idList = ArrayUtil.toList(statusIds);
			for (Long id : idList) {
				statusService.delete(id);
			}
			return new ResultModel(Constants.SUCCESS, "删除成功");
		}
		return new ResultModel(Constants.FAIL, "删除失败");
	}

	
	/**
	 * 获取所有状态信息
	 * @return
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult list(int limit,int page){
		startPage(page,limit);
		List<Status> statusList;
		statusList=statusService.listAll();
		return getDataTable(statusList);
	}

	/**
	 * 根据名称获取（模糊搜索）
	 * @return
	 */
	@RequestMapping(value="/search",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult search(int limit,int page,@RequestParam("statusName")String statusName){
		startPage(page,limit);
		List<Status> list=statusService.search(statusName);
		return getDataTable(list);
	}

	/**
	 * 根据id获取
	 * @param statusId
	 * @return
	 */
	@RequestMapping(value="/get",method=RequestMethod.GET)
	@ResponseBody
	public Status get(@RequestParam("statusId")Long statusId){
		return statusService.get(statusId);
	}
}
