package com.scau.controller;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.model.OSystem;
import com.scau.service.OSystemService;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/osystem")
public class OSystemController extends BaseController{
    @Autowired
    private OSystemService oSystemService;

    /**
     * 添加操作系统
     *
     * @param operatingSystem
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel add(@RequestParam("operatingSystem") String operatingSystem) {
        if (oSystemService.selectByOSystemName(operatingSystem) == null) {
            OSystem osystem = new OSystem();
            osystem.setOperatingSystem(operatingSystem);
            long id = oSystemService.add(osystem);
            if (id > 0) {
                osystem.setId(id);
            }
            return new ResultModel(Constants.SUCCESS, "添加成功", osystem);
        } else {
            return new ResultModel(Constants.FAIL, "添加失败,已存在该浏览器");
        }

    }

    /**
     * 修改操作系统
     *
     * @param operatingSystem
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel update(@RequestParam("osystemId") Long osystemId,
                              @RequestParam("operatingSystem") String operatingSystem) {
        boolean result = false;
        OSystem osystem = oSystemService.get(osystemId);
        if (osystem != null) {//该浏览器存在
            osystem.setOperatingSystem(operatingSystem);
            if (oSystemService.update(osystem) == 1) {
                return new ResultModel(Constants.SUCCESS, "修改成功");
            }
        }
        return new ResultModel(Constants.FAIL, "修改失败");
    }

    /**
     * 删除类型
     *
     * @param osystemIds
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel delete(@RequestParam("ids") String osystemIds) {
        if (osystemIds != null) {
            List<Long> idList = ArrayUtil.toList(osystemIds);
            for (Long osystemId : idList) {
                OSystem osystem = oSystemService.get(osystemId);
                oSystemService.delete(osystem);
            }
            return new ResultModel(Constants.SUCCESS, "删除成功");
        }
        return new ResultModel(Constants.FAIL, "删除失败");
    }

    /**
     * 获取所有操作系统
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult list(int limit, int page) {
        startPage(page, limit);
        List<OSystem> osystemList = oSystemService.listAll();
        return getDataTable(osystemList);
    }

    /**
     * 根据系统名称获取（模糊搜索）
     *
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult search(int limit, int page, @RequestParam("operatingSystem") String operatingSystem) {
        startPage(page, limit);
        List<OSystem> osystemList = oSystemService.search(operatingSystem);
        return getDataTable(osystemList);
    }

    /**
     * 根据id获取
     *
     * @param osystemId
     * @return
     */
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public OSystem get(@RequestParam("osystemId") Long osystemId) {
        return oSystemService.get(osystemId);
    }
}
