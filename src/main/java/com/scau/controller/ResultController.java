package com.scau.controller;

import java.util.List;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.scau.model.Result;
import com.scau.service.ResultService;


@Controller
@RequestMapping("/result")
public class ResultController extends BaseController{

	@Autowired
	private ResultService resultService;
	
	/**
	 * 添加执行结果
	 * @param resultName
	 * @return
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel add(@RequestParam("resultName")String resultName){
		Result result=null;
		if(resultService.selectByName(resultName)==null){
				result=new Result();
				result.setResultName(resultName);
				long id=resultService.add(result);
				if(id>0){
					result.setId(id);
					return new ResultModel(Constants.SUCCESS,"添加成功",result);
				}
		    }
		return new ResultModel(Constants.FAIL,"添加失败，已存在该执行结果");
	}
	
	/**
	 * 修改执行结果
	 * @param resultId
	 * @param resultName
	 * @return
	 */
	@RequestMapping(value="/update",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel update(@RequestParam("resultId")Long resultId,@RequestParam("resultName")String resultName){
		Result result=resultService.get(resultId);
		if(resultName!=null){
			result.setResultName(resultName);
			if(resultService.update(result)==1){
				return new ResultModel(Constants.SUCCESS, "修改成功",result);
			}
		}
		return new ResultModel(Constants.FAIL, "修改失败");
	}
	
	/**
	 * 删除执行结果
	 * @param resultIds
	 * @return
	 */
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel delete(@RequestParam("ids")String resultIds ) {
		if (resultIds != null) {
			List<Long> idList = ArrayUtil.toList(resultIds);
				for (int i = 0; i < idList.size(); i++) {
					Long resultId = idList.get(i);
					if (resultId != null) {
						if (resultService.delete(resultId) != 1) {
							return new ResultModel(Constants.FAIL, "删除失败");
						}
					}
				}
			return new ResultModel(Constants.SUCCESS,"删除成功");
		}
		return new ResultModel(Constants.FAIL, "删除失败");
	}
	
	/**
	 * 获取执行结果列表
	 * @return
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult list(int limit, int page){
		startPage(page,limit);
		List<Result> resultList=resultService.listAll();
		return getDataTable(resultList);
	}

	/**
	 * 根据结果名获取（模糊搜索）
	 * @return
	 */
	@RequestMapping(value="/search",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult search(int limit,int page,@RequestParam("resultName")String resultName){
		startPage(page,limit);
		List<Result> list=resultService.search(resultName);
		return getDataTable(list);
	}

	/**
	 * 根据id获取
	 * @param resultId
	 * @return
	 */
	@RequestMapping(value="/get",method=RequestMethod.GET)
	@ResponseBody
	public Result get(@RequestParam("resultId")Long resultId){
		return resultService.get(resultId);
	}
}