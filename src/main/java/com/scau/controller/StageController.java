package com.scau.controller;

import java.util.List;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

import com.scau.model.Stage;
import com.scau.service.StageService;

@Controller
@RequestMapping("/stage")
public class StageController extends BaseController {
	
	@Autowired
	private StageService stageService;
	
	/**
	 * 添加适用阶段
	 * @param stageName
	 * @return
	 */
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel add(@RequestParam("stageName")String stageName){
		Stage stage=null;
		if(stageService.selectByName(stageName)==null){
				stage =new Stage();
				stage.setStageName(stageName);
				//添加并返回id
				long id=stageService.add(stage);
				if(id>0){
					stage.setId(id);
				}
				return new ResultModel(Constants.SUCCESS,"添加成功",stage);
			}
		return new ResultModel(Constants.FAIL,"添加失败,已存在该适用阶段");
	}
	
	/**
	 * 修改适用阶段名称
	 * @param stageId
	 * @param stageName
	 * @return
	 */
	@RequestMapping(value="/update",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel update(@RequestParam("stageId")Long stageId,
							  @RequestParam("stageName")String stageName){
		boolean result =false;
		Stage stage=stageService.get(stageId);
		if(stage!=null) {
			stage.setStageName(stageName);
			if (stageService.update(stage) == 1)
				return new ResultModel(Constants.SUCCESS, "修改成功",stage);
		}
		return new ResultModel(Constants.SUCCESS, "修改失败");
	}
	
	/**
	 * 删除适用阶段
	 * @param stageIds
	 * @return
	 */
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel delete(@RequestParam("ids")String stageIds ) {
		if (stageIds != null) {
			List<Long> idList = ArrayUtil.toList(stageIds);
	        for(Long stageId:idList){
				if(stageService.delete(stageId)!=1)
					return new ResultModel(Constants.FAIL, "删除失败");
			}
			return new ResultModel(Constants.SUCCESS,"删除成功");
		}
		return new ResultModel(Constants.SUCCESS, "删除失败");
	}
	
	/**
	 * 获取所有适用阶段
	 * @return
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult list(int limit, int page){
		startPage(page,limit);
		List<Stage> list=stageService.listAll();
		return getDataTable(list);
	}

	/**
	 * 根据名称获取（模糊搜索）
	 * @return
	 */
	@RequestMapping(value="/search",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult search(int limit,int page,@RequestParam("stageName")String stageName){
		startPage(page,limit);
		List<Stage> list=stageService.search(stageName);
		return getDataTable(list);
	}

	/**
	 * 根据id获取
	 * @param stageId
	 * @return
	 */
	@RequestMapping(value="/get",method=RequestMethod.GET)
	@ResponseBody
	public Stage get(@RequestParam("stageId")Long stageId){
		return stageService.get(stageId);
	}

}
