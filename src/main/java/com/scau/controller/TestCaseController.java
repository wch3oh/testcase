package com.scau.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.github.pagehelper.PageInfo;
import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.service.*;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import com.scau.util.SessionUtil;
import com.scau.vo.TestCaseVo;
import org.aspectj.apache.bcel.classfile.ConstantMethodHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scau.model.*;
import com.scau.vo.RecordVo;


@Controller
@RequestMapping("/testcase")
public class TestCaseController extends BaseController {

    @Autowired
    private TestCaseService testcaseService;
    @Autowired
    private RecordService recordService;
    @Autowired
    private HttpServletRequest request;

    /**
     * 添加用例/编辑用例
     *
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel add(TestCase testcase) {
        User user = SessionUtil.getUserSession(request);//获取当前登录用户
        if (user != null) {//用户处于登录状态
            if (user.getAuthorityFlag() == Constants.AUTHORITY) {//判断用户是否有权限
                if (testcase.getId() == null) {//添加
                    if (testcaseService.selectByTitle(testcase.getTitle(), Constants.DEFAULT).size() != 0) {
                        return new ResultModel(Constants.FAIL, "添加失败，已有相同标题的用例");
                    } else {
                        testcase.setUserId(user.getId());//创建者id
                        testcase.setUserId2(user.getId());
                        if (testcaseService.add(testcase) > 0) {
                            return new ResultModel(Constants.SUCCESS, "添加成功", testcase);
                        }
                    }
                } else {//编辑
                    TestCaseVo t = testcaseService.getByTestcaseId(testcase.getId());
                    testcase.setExecuteTime(t.getExecuteTime());
                    if (testcaseService.update(testcase) > 0) {
                        return new ResultModel(Constants.SUCCESS, "修改成功", testcase);
                    }
                    return new ResultModel(Constants.FAIL, "修改失败");
                }
            }
            return new ResultModel(Constants.FAIL, "操作失败，无权限-添加权限请联系管理员");
        }
        return new ResultModel(Constants.SUCCESS, "操作失败,登录状态失效");
    }

    /**
     * 复制用例
     *
     * @param testcase
     * @return
     */
    @RequestMapping(value = "/copy", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel copy(TestCase testcase) {
        User user = SessionUtil.getUserSession(request);
        if (user != null) {//用户处于登录状态
            if (user.getAuthorityFlag() == Constants.AUTHORITY) {    //判断用户是否有权限
                testcase.setUserId(user.getId());
                testcase.setUserId2(user.getId());
                if (testcaseService.selectByTitle(testcase.getTitle(), Constants.DEFAULT).size() == 0) {
                    if (testcaseService.add(testcase) > 0) {
                        return new ResultModel(Constants.SUCCESS, "添加成功", testcase);
                    }
                } else {
                    return new ResultModel(Constants.FAIL, "添加失败，已存在相同标题的用例");
                }
            }
            return new ResultModel(Constants.FAIL, "操作失败，无权限-添加权限请联系管理员");
        } else {
            return new ResultModel(Constants.FAIL, "添加失败");
        }
    }


    /**
     * 更改flag
     *
     * @param testcaseIds
     * @param session
     * @return
     */
    @RequestMapping(value = "/updateFlag", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel updateFlag(@RequestParam("ids") String testcaseIds, HttpSession session) {
        boolean result = false;
        User user = SessionUtil.getUserSession(request);
        if (user != null) {//用户处于登录状态
            if (user.getAuthorityFlag() == Constants.AUTHORITY) {//判断用户是否有权限
                if (testcaseIds != null) {
                    List<Long> idList = ArrayUtil.toList(testcaseIds);
                    for (Long testcaseId:idList) {
                        TestCase testcase = testcaseService.selectByTestcaseId(testcaseId);
                        if (testcase.getFlag() == Constants.DEFAULT) {
                            testcase.setFlag(Constants.RECYCLE);
                            testcaseService.updateFlag(testcase);
                        }else if (testcase.getFlag() == Constants.RECYCLE) {
                            testcase.setFlag(Constants.DEFAULT);
                            testcaseService.updateFlag(testcase);
                        }
                    }
                    return new ResultModel(Constants.SUCCESS, "操作成功");
                }
            } else {
                return new ResultModel(Constants.FAIL, "操作失败-无权限,添加权限请联系管理员");
            }
        }
        return new ResultModel(Constants.FAIL, "操作失败！");
    }

    /**
     * 执行测试用例，添加执行记录,更新测试用例信息
     *
     * @param testcaseId
     * @param resultId
     * @param truth
     * @return
     */
    @RequestMapping(value = "/execute", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel updaeResult(@RequestParam("testcaseId") Long testcaseId, @RequestParam("resultId") Long resultId,
                                   @RequestParam("truth") String truth) {
        User user = SessionUtil.getUserSession(request);
        if (user != null) {//用户处于登录状态
            if (user.getAuthorityFlag() == Constants.AUTHORITY) {//判断用户是否有权限
                TestCase testcase = testcaseService.selectByTestcaseId(testcaseId);
                if (testcase != null) {//该用例存在
                    Record record = new Record();
                    record.setResultId(resultId);
                    record.setTestcaseId(testcaseId);
                    record.setTruth(truth);
                    record.setUserId(user.getId());
                    record.setExpectation(testcase.getExpectation());

                    Long recordId = recordService.add(record);
                    if (recordId > 0) {//添加执行记录成功
                        record = recordService.get(recordId);
                        testcase.setResultId(resultId);//更新用例的执行结果
                        testcase.setUserId2(user.getId());//更新执行者id
                        testcase.setExecuteTime(record.getUpdateTime());//更新最后执行时间
                        testcase.setNum(testcase.getNum() + 1);//更新执行次数
                        if (testcaseService.update(testcase) == Constants.SUCCESS) {//更新执行的测试用例信息
                            return new ResultModel(Constants.SUCCESS, "执行成功",testcase);
                        }
                    }
                }
            } else {
                return new ResultModel(Constants.FAIL, "执行失败，无操作权限-添加权限请联系管理员");
            }
        }
        return new ResultModel(Constants.FAIL, "操作失败");
    }

    /**
     * 删除测试用例
     *
     * @param testcaseIds
     * @param session
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel delete(@RequestParam("ids") String testcaseIds, HttpSession session) {
        User user = SessionUtil.getUserSession(request);//获取当前登录用户
        if (user != null) {//用户处于登录状态
            if (user.getAuthorityFlag() == Constants.AUTHORITY) {//判断用户是否有权限,1表示有权限
                if (testcaseIds != null) {
                    List<Long> idList = ArrayUtil.toList(testcaseIds);
                    for (int i = 0; i < idList.size(); i++) {
                        Long testcaseId = idList.get(i);
                        testcaseService.delete(testcaseId);
                    }
                    return new ResultModel(Constants.SUCCESS, "删除成功");
                }
            } else {
                return new ResultModel(Constants.FAIL, "删除失败-无权限，添加权限请联系管理员！");
            }
        }
        return new ResultModel(Constants.FAIL, "删除失败");
    }


    /**
     * 获取某测试用例的执行记录
     *
     * @param testcaseId
     * @return
     */
    @RequestMapping(value = "/listRecord", method = RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult listRecord(@RequestParam("testcaseId") Long testcaseId) {
        List<RecordVo> list = recordService.list(testcaseId);
        LayuiGridDataResult result = new LayuiGridDataResult();
        result.setData(list);
        result.setCount(new PageInfo(list).getTotal());
        return result;

    }

    /**
     * 获取所有测试用例
     *
     * @param flag
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult list(int limit, int page, @RequestParam(value = "flag", required = false) Integer flag) {
        startPage(page, limit);
        List<TestCaseVo> testcaseVoList = testcaseService.list(flag);
        return getDataTable(testcaseVoList);
    }


    /**
     * 根据用例id获取
     *
     * @param testcaseId
     * @return
     */
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public ResultModel get(@RequestParam("testcaseId") Long testcaseId) {
        TestCaseVo testcaseVo = testcaseService.getByTestcaseId(testcaseId);
        return new ResultModel(Constants.SUCCESS, testcaseVo);
    }

    /**
     * 搜索
     * @return
     */
    @RequestMapping(value = "/selectByPage", method = RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult selectByPage(int limit, int page, @RequestParam("according") Integer according,
                                            @RequestParam(value = "flag", required = false) Integer flag,
                                            @RequestParam(value = "title", required = false) String title,
                                            @RequestParam(value = "id", required = false) Long id) {
        startPage(page, limit);
        List<TestCaseVo> testcaseVoList = new ArrayList<TestCaseVo>();
        if (id!=null && id > 0) {
            switch (according) {
                case Constants.USER: {
                    testcaseVoList = testcaseService.listByUserId(title, id, flag);
                    break;
                }
                case Constants.PRIORITY: {
                    testcaseVoList = testcaseService.listByPriorityId(title, id, flag);
                    break;
                }
                case Constants.PROJECT: {
                    testcaseVoList = testcaseService.listByProjectId(title, id, flag);
                    break;
                }
                case Constants.MODULE: {
                    testcaseVoList = testcaseService.listByModuleId(title, id, flag);
                    break;
                }
                case Constants.REQUIREMENT: {
                    testcaseVoList = testcaseService.listByRequirementId(title, id, flag);
                    break;
                }
                case Constants.RESULT: {
                    testcaseVoList = testcaseService.listByResultId(title, id, flag);
                    break;
                }
                case Constants.STAGE: {
                    testcaseVoList = testcaseService.listByStageId(title, id, flag);
                    break;
                }
                case Constants.STATUS: {
                    testcaseVoList = testcaseService.listByStatusId(title, id, flag);
                    break;
                }
                case Constants.TESCASE: {
                    testcaseVoList = testcaseService.listByTypeId(title, id, flag);
                    break;
                }
            }
        } else {
            testcaseVoList = testcaseService.listByTitle(title, flag);
        }
        return getDataTable(testcaseVoList);
    }

}

