package com.scau.controller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.scau.model.User;
import com.scau.service.UserService;


@Controller
@RequestMapping("/user")
public class UserController extends BaseController{

	@Autowired
	public UserService userService;
	@Autowired
	private HttpServletRequest request;


	
	/**
	 * 用户登录
	 * @param account
	 * @param password
	 * @return
	 */
	@RequestMapping(value="/login",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel login(@RequestParam("account") String account,
	                       @RequestParam("password") String password) {
		User user=userService.search(account, PasswordUtils.encodeByMd5(password));
		if(user!=null) {//账号密码正确时允许登录
			SessionUtil.setUserSession(request,user);
			if(user.getPositionFlag()==Constants.ADMIN){
				return new ResultModel(Constants.SUCCESS,"admin.html");
			}else if(user.getPositionFlag()==Constants.TESTER){
				return new ResultModel(Constants.SUCCESS,"index.html");
			}else if(user.getPositionFlag()==Constants.PRODUCT_MANAGER){
				return new ResultModel(Constants.SUCCESS,"pmIndex.html");
			}else if(user.getPositionFlag()==Constants.PROGRAMMER){
				return new ResultModel(Constants.SUCCESS,"programmerIndex.html");
			} else if(user.getPositionFlag()==Constants.NORMAL){
				return new ResultModel(Constants.SUCCESS,"index.html");
			}
		}
		return new ResultModel(Constants.FAIL,"账号或密码错误");
	}


	/**
	 * 用户注册
	 * @param account
	 * @param password
	 * @param nickName
	 * @return
	 */
	@RequestMapping(value="/register",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel register(@RequestParam("account") String account,
								@RequestParam("password") String password,
								@RequestParam("nickName") String nickName) {
		User user=userService.search(account);
		if(user==null) {//账号不存在时允许注册
			user=new User();
			user.setAccount(account);
			user.setNickname(nickName);
			user.setPositionFlag(Constants.NORMAL);//普通用户
			user.setPassword(PasswordUtils.encodeByMd5(password));
			user.setName(nickName);//默认为昵称
			user.setImage(Constants.IMAGE);//默认头像
			if(userService.add(user)>0) {
				return new ResultModel(Constants.SUCCESS, "注册成功", user);
			}
		}
		return new ResultModel(Constants.FAIL,"注册失败，账号已存在！");
	}
	
	/**
	 * 用户退出登录
	 * @return
	 */
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	@ResponseBody
	public ResultModel logout() {
		User user=SessionUtil.getUserSession(request);
		if(user!=null){
			SessionUtil.removeUserSession(request);
		}
		return new ResultModel(Constants.SUCCESS,"login.html");
	}
	

	/**
	 * 更新用户头像
	 * @param file
	 * @return
	 */
	    @RequestMapping(value = "/upload",method = RequestMethod.POST)
	    @ResponseBody
	    public ResultModel upload(@RequestParam("file")MultipartFile file){
	        String url=null;
//	        String path = request.getServletContext().getRealPath("/");
			String path="C:\\Users\\jiachun\\Documents\\MyEclipse 2017 CI\\testcase\\src\\main\\webapp\\images\\";//本地路径
	        String fileName=file.getOriginalFilename();
	        String suffix=fileName.substring(fileName.lastIndexOf(".")+1);
	        //图片名：系统时间+随机数
	        fileName=System.currentTimeMillis()+new Random().nextInt(10000)+"."+suffix;
	        File dir = new File(path, fileName);
	        if(!dir.exists()){
	            dir.mkdirs();
	        }
	        try {
	            file.transferTo(dir);
	            //图片的相对路径
	            url= File.separator+Paths.get(Constants.UPLOAD_ROOT,fileName);
	            url=url.replace("\\","/");
	            User user=SessionUtil.getUserSession(request);//获取当前登录用户
	    		if(user!=null) {//用户为登录状态时允许更改
	    			user.setImage(url);
	    			if(userService.update(user)==1){
	    				return new ResultModel(Constants.SUCCESS,"头像更新成功",user);
	    			}
	    		}
	        }catch (IOException e){
				return new ResultModel(Constants.FAIL,e.toString());
	        }
			return new ResultModel(Constants.FAIL,"操作失败");
	    }
	
	/**管理员
	 * 根据userId修改用户权限
	 * @param userId
	 * @return
	 */
	@RequestMapping(value="/update/authorityFlag",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel updateFlag(@RequestParam("id")Long userId){
		User user =userService.get(userId);
		if(user.getAuthorityFlag()==Constants.NO_AUTHORITY){
			user.setAuthorityFlag(Constants.AUTHORITY);
			if(userService.update(user)==Constants.SUCCESS){
				return new ResultModel(Constants.SUCCESS,"开启权限成功");
			}
		}else if(user.getAuthorityFlag()==Constants.AUTHORITY){
			user.setAuthorityFlag(Constants.NO_AUTHORITY);
			if(userService.update(user)==Constants.SUCCESS){
				return new ResultModel(Constants.SUCCESS,"关闭权限成功");
			}
		}
		return new ResultModel(Constants.FAIL,"修改失败");
	}
	/**
	 * 修改密码
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 */
	@RequestMapping(value="/update/password",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel updatePassword(@RequestParam("oldPassword") String oldPassword,
									@RequestParam("newPassword") String newPassword) {
		User user=SessionUtil.getUserSession(request);
		//用户为登录状态且旧密码输入正确时允许更改
		if(user!=null&&user.getPassword().equals(PasswordUtils.encodeByMd5(oldPassword))) {
			user.setPassword(PasswordUtils.encodeByMd5(newPassword));
			if(userService.updatePassword(user)==Constants.SUCCESS) {
				return new ResultModel(Constants.FAIL,"修改成功");
			}
		}
		return new ResultModel(Constants.FAIL,"修改失败，旧密码错误");
	}
	
	/**管理员
	 * 获取所有用户信息
	 * @return
	 */
	@RequestMapping(value="/list",method=RequestMethod.GET)
	@ResponseBody
	public LayuiGridDataResult list(int limit, int page, @RequestParam(value = "name",required = false)String name,
											@RequestParam(value = "sex",required = false)String sex,
											@RequestParam(value = "authorityFlag",required = false)Integer authorityFlag){
		startPage(page,limit);
		List<User> userList = userService.list(name,sex,authorityFlag);
		return getDataTable(userList);
	}

	/**
	 * 获取当前登录用户的信息
	 * @return
	 */
	@RequestMapping(value="/get",method=RequestMethod.GET)
	@ResponseBody
	public ResultModel get() {
		User user=SessionUtil.getUserSession(request);
		if(user!=null){
			return new  ResultModel(Constants.SUCCESS,"获取当前登录用户信息成功",user);
		}
		return new ResultModel(Constants.FAIL,"当前无用户登录");
	}

	/**
	 * 根据id获取
	 * @param userId
	 * @return
	 */
	@RequestMapping(value="/selectById",method=RequestMethod.GET)
	@ResponseBody
	public ResultModel selectByUserId(@RequestParam("id")Long userId){
		User user=userService.get(userId);
		if(user!=null){
			return new ResultModel(Constants.SUCCESS,user);
		}
		return new ResultModel(Constants.FAIL,"获取失败");
	}

	/**
	 * 根据id删除
	 * @param userIds
	 * @return
	 */
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@ResponseBody
	public ResultModel delete(@RequestParam("ids")String userIds){
		if(userIds!=null) {
			List<Long> idList=ArrayUtil.toList(userIds);
			for(Long id:idList){
				if (userService.delete(id)!=1){
					return new ResultModel(Constants.FAIL,"删除失败");//删除用户
				}
			}
			return new ResultModel(Constants.SUCCESS,"删除成功");
		}
		return new ResultModel(Constants.FAIL,"删除失败");
	}

	/**
	 *添加/编辑用户
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/add",method = RequestMethod.POST)
	@ResponseBody
	public ResultModel add(User user) {
		if (user.getId() == null) {//添加
			user.setPassword(PasswordUtils.encodeByMd5(Constants.ORIGINAL_PASS));//初始密码
			user.setImage(Constants.IMAGE);
			if (userService.search(user.getAccount()) != null) {
				return new ResultModel(Constants.FAIL, "添加失败，账号已存在");
			}else if(userService.add(user)>0){
				return new ResultModel(Constants.FAIL,"添加成功",user);
			}
				return new ResultModel(Constants.FAIL,"添加失败");
			} else {//编辑
			if (userService.update(user) == Constants.SUCCESS) {
					return new ResultModel(Constants.SUCCESS, "修改成功",user);
				}
			}
			return new ResultModel(Constants.FAIL, "修改失败");
		}
}
