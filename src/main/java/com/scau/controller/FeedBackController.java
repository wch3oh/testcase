package com.scau.controller;

import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.model.FeedBack;
import com.scau.model.User;
import com.scau.service.FeedBackService;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import com.scau.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/feedback")
public class FeedBackController extends  BaseController{

    @Autowired
    private FeedBackService feedbackService;
    @Autowired
    private HttpServletRequest request;

    /**
     * 添加/编辑反馈
     * @param feedback
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel add(FeedBack feedback) {
        if(feedback.getId()==null){//添加
            User user = SessionUtil.getUserSession(request);//获取当前登录用户
            if(user!=null){
                feedback.setUserId(user.getId());
                if(feedbackService.add(feedback)>0){
                    return new ResultModel(Constants.SUCCESS,"提交反馈成功",feedback);
                }
                    return new ResultModel(Constants.FAIL,"提交反馈失败");
            }
            return new ResultModel(Constants.FAIL,"操作失败，当前登录状态已失效");
        }else {//编辑
            if(feedbackService.update(feedback)==Constants.SUCCESS){
                return new ResultModel(Constants.SUCCESS,"修改成功",feedback);
            }
            return new ResultModel(Constants.FAIL,"修改失败");
        }
    }

    /**
     * 删除反馈
     * @param feedbackIds
     * @return
     */
    @RequestMapping(value="/delete",method=RequestMethod.POST)
    @ResponseBody
    public ResultModel delete(@RequestParam("ids")String feedbackIds ) {
        if (feedbackIds != null) {
            List<Long> idList = ArrayUtil.toList(feedbackIds);
            for(Long id:idList){
                if(feedbackService.delete(id)!=Constants.SUCCESS)
                    return new ResultModel(Constants.FAIL, "删除失败");
            }
            return new ResultModel(Constants.SUCCESS,"删除成功");
        }
        return new ResultModel(Constants.SUCCESS, "删除失败");
    }

    /**
     * 获取用户反馈
     * @return
     */
    @RequestMapping(value="/list",method=RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult list(int limit, int page,@RequestParam(value = "userId",required = false)Long userId,
                                    @RequestParam(value = "flag",required = false)Integer flag){
        startPage(page,limit);
        List<FeedBack> list=feedbackService.list(userId,flag);
        return getDataTable(list);
    }

    /**
     * 根据id获取
     * @param id
     * @return
     */
    @RequestMapping(value="/get",method=RequestMethod.GET)
    @ResponseBody
    public ResultModel get(@RequestParam("id")Long id){
      FeedBack feedback=feedbackService.get(id);
      return new ResultModel(Constants.SUCCESS,feedback);
    }


}
