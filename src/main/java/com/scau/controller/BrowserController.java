package com.scau.controller;


import com.scau.dto.LayuiGridDataResult;
import com.scau.dto.ResultModel;
import com.scau.model.Browser;
import com.scau.service.BrowserService;
import com.scau.util.ArrayUtil;
import com.scau.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/browser")
public class BrowserController extends BaseController {

    @Autowired
    private BrowserService browserService;

    /**
     * 添加浏览器
     *
     * @param browserName
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel add(@RequestParam("browserName") String browserName) {
        if (browserService.selectByBrowserName(browserName) == null) {
            Browser browser = new Browser();
            browser.setBrowserName(browserName);
            long id = browserService.add(browser);
            if (id > 0) {
                browser.setId(id);
            }
            return new ResultModel(Constants.SUCCESS, "添加成功", browser);
        } else {
            return new ResultModel(Constants.FAIL, "添加失败,已存在该浏览器");
        }

    }

    /**
     * 修改浏览器名
     *
     * @param browserName
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel update(@RequestParam("browserId") Long browserId,
                              @RequestParam("browserName") String browserName) {
        boolean result = false;
        Browser browser = browserService.get(browserId);
        if (browser != null) {//该浏览器存在
            browser.setBrowserName(browserName);
            if (browserService.update(browser) == 1) {
                return new ResultModel(Constants.SUCCESS, "修改成功");
            }
        }
        return new ResultModel(Constants.FAIL, "修改失败");
    }

    /**
     * 删除类型
     *
     * @param browserIds
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel delete(@RequestParam("ids") String browserIds) {
        if (browserIds != null) {
            List<Long> idList = ArrayUtil.toList(browserIds);
            for (Long browserId : idList) {
                Browser browser = browserService.get(browserId);
                browserService.delete(browser);
            }
            return new ResultModel(Constants.SUCCESS, "删除成功");
        }
        return new ResultModel(Constants.FAIL, "删除失败");
    }

    /**
     * 获取所有浏览器
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult list(int limit, int page) {
        startPage(page, limit);
        List<Browser> browserList = browserService.listAll();
        return getDataTable(browserList);
    }

    /**
     * 根据类型名获取（模糊搜索）
     *
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public LayuiGridDataResult search(int limit, int page, @RequestParam("browserName") String browserName) {
        startPage(page, limit);
        List<Browser> browserList = browserService.search(browserName);
        return getDataTable(browserList);
    }

    /**
     * 根据id获取
     *
     * @param browserId
     * @return
     */
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public Browser get(@RequestParam("browserId") Long browserId) {
        return browserService.get(browserId);
    }
}
