package com.scau.util;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SqlSessionUtils {
	private final static SqlSessionFactory SQL_SESSION_FACTORY;

	static {
		String resource ="mybatis-config.xml";
		InputStream inputStream = null;
		try {
			inputStream = Resources.getResourceAsStream(resource);
		} catch (IOException e) {
			Logger.getLogger(SqlSessionUtils.class.getName()).log(Level.SEVERE, null, e);
		}
		SQL_SESSION_FACTORY = new SqlSessionFactoryBuilder().build(inputStream);
	}

	private SqlSessionUtils() {
		throw new AssertionError();
	}

	/**
	 * 获取工厂类
	 *
	 * @return SqlSessionFactory
	 */
	public static SqlSessionFactory getFactory() {
		return SQL_SESSION_FACTORY;
	}

	/**
	 * 检查是否包含数据库非法字符
	 *
	 * @param targets String[]
	 * @return boolean
	 */
	public static boolean existForbiddenChar(String[] targets) {
		String[] forbidden = {"\\", "$", "|", "%", "_", "(", ")", "*", "+", ".", "[", "]", "?", "^", "{", "}"};
		for (String i : forbidden) {
			for (String j : targets) {
				boolean checkResult = j.contains(i);
				if (checkResult) {
					return true;
				}
			}
		}
		return false;
	}

}
