package com.scau.util;


public class Constants {
    public static final int SUCCESS=1;//操作成功
    public static final int FAIL=0;//操作失败

    public static final int NORMAL=0;//普通用户
    public static final int ADMIN=1;//管理员
    public static final int TESTER=2;//测试人员
    public static final int PRODUCT_MANAGER=3;//产品经理
    public static final int PROGRAMMER=4;//开发人员

    public static final int AUTHORITY=1;//有权限
    public static final int NO_AUTHORITY=0;//无权限
    public static final int DEFAULT=0;//正常
    public static final int RECYCLE=1;//回收站

    public  static final int USER=1;
    public  static final int PRIORITY=2;
    public  static final int PROJECT=3;
    public  static final int MODULE=4;
    public  static final int REQUIREMENT=5;
    public  static final int TESCASE=6;
    public  static final int STAGE=7;
    public  static final int STATUS=8;
    public  static final int RESULT=9;

    public static final String UPLOAD_ROOT="images";
    public static final String ORIGINAL_PASS="000000";//默认密码
    public static final String IMAGE="/images/image.jpg";//默认头像
}
