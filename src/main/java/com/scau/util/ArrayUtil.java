package com.scau.util;

import java.util.ArrayList;
import java.util.List;

//将前端返回的ids字符串转化成long型并存储在列表中，供后端使用
public class ArrayUtil {

    public static List<Long> toList(String ids){
        if(ids==null){
            return new ArrayList<>();
        }
        String[] array=ids.split(",");
        List<Long> list=new ArrayList<>();
        for(int i=0;i<array.length;i++){
            list.add(Long.valueOf(array[i]));
        }
        return list;
    }

}
