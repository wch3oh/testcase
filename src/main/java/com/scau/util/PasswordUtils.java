package com.scau.util;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class PasswordUtils {

	private PasswordUtils() {
		throw new AssertionError();
	}

	/**
	 * MD5加密
	 *
	 * @param password String
	 * @return String
	 * @throws NoSuchAlgorithmException     Exception
	 * @throws UnsupportedEncodingException Exception
	 */
	public static String encodeByMd5(String password)  {
		
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
			Base64.Encoder baseEncoder = Base64.getEncoder();
			try {
				return baseEncoder.encodeToString(md5.digest(password.getBytes("utf-8")));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	public static boolean match(String password, String passwordMd5) {
			return encodeByMd5(password).equals(passwordMd5);
	}

	public static  void main(String[] args){
		System.out.println(encodeByMd5("123456").equals("4QrcOUm6Wau+VuBX8g+IPg=="));
	}
}
