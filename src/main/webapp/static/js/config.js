var api = 'http://localhost:8080/';

var nickNameDefault = 'Vistor';

function backToLoginPage () {
  window.location.href = 'login.html';
}

function checkAccess (app) {
  var access = false;
  $.ajax({url: api + 'user/get', success: function (resp) {
      if (resp.flag != undefined) {
        access = true;
        app.user.nickName = resp.nickname;
        app.user.flag = resp.flag;
		if(resp.flag===1){
			 window.location.href = 'admin.html';
		}
      }
      if (!access) {
        backToLoginPage();
      }
    }, error: function () {
      backToLoginPage();
  }})
}

function timestampToTime(timestamp) {
    var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear() + '-';
    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    D = date.getDate()<10?('0'+date.getDate()):(date.getDate())+ ' ';
    h = date.getHours()<10?('0'+date.getHours()):(date.getHours()) + ':';
    m = date.getMinutes()<10?('0'+date.getMinutes()):(date.getMinutes()) + ':';
    s = date.getSeconds()<10?('0'+date.getSeconds()):(date.getSeconds());
    return Y+M+D;
}

function quit () {
  $.get(api + 'user/logout', {}, function () {
    backToLoginPage();
  })
}
