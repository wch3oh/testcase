function timestampToTime(timestamp) {
    var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear();
    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) ;
    D = date.getDate()<10?('0'+date.getDate()):(date.getDate());
    h = date.getHours()<10?('0'+date.getHours()):(date.getHours()) ;
    m = date.getMinutes()<10?('0'+date.getMinutes()):(date.getMinutes()) ;
    s = date.getSeconds()<10?('0'+date.getSeconds()):(date.getSeconds());
    return Y+"-"+M+"-"+D+" "+h+":"+m+":"+s;
}
function getUrlParam(name)
{
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg)||"";
    if(r!=null){
        return unescape(r[2]);
    }else{
        return null;
    }
}

function isPhone(phone)
{
    var phoneReg = /^1[3-578]\d{9}$/;
    if(phoneReg.test(phone)){
        return true;
    }else{
        return false;
    }

}

function isEmail(email)
{
    var reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
    if(reg.test(email)){
        return true;
    }else{
        return false;
    }
}